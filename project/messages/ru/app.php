<?php

return [
    'NAV_HOME' => 'Главная',
    'NAV_CONTACT' => 'СВЯЗАТСЯ С НАМИ',
    'NAV_SIGNUP' => 'Регистрация',
    'NAV_LOGIN' => 'Вход',
    'NAV_PROFILE' => 'Личный кабинет',
    'NAV_LOGOUT' => 'Выход',
    'NAV_ADMIN' => 'Управление',
    'NAV_ADMIN_USERS' => 'Клиент',
    'User ID'=>'ID user',
    'Driver ID'=>'ID driver',
    'Created At'=>'НОМЕР ВОДИТЕЛЯ',
    'Updated At'=>'ВРЕМЯ СОЗДАНИЯ ЗАКАЗА',
    'From Country'=>'СТРАНА',
    'From City'=>'ГОРОД',
    'From Street'=>'УЛИЦА',
    'From Paradniy'=>'ПОДЪЕЗД',
    'From Time'=>'УДОБНОЕ ВРЕМЯ',
    'From Desc'=>'ПРИМЕЧАНИЕ',
    'Plus Money'=>'ДОБАВИТЬ К ЗАКАЗУ',

    'NAV_ZAKAZ_ADMIN_MY' => 'Заказы в работе',
    'NAV_ZAKAZ_ADMIN' => 'Ожидающие заказы',
    'ADMIN_USERS' => 'Пользователи',
    'NAV_ZAKAZ_ADMIN_TOUA' => 'В Украину',
    'NAV_ZAKAZ_ADMIN_TOUK' => 'В Англию',
    'ADMIN_UPRAVLENIE' => 'Панель управления',

    'Novie_veshi_short' => 'Новые вещи?',


    'Tel'=>'ТЕЛЕФОН',
    'Name'=>'ИМЯ',
    'To County'=>'СТРАНА',
    'To City'=>'ГОРОД',
    'To Street'=>'УЛИЦА',
    'To Paradniy'=>'ПОДЪЕЗД',
    'To Desc'=>'ПРИМЕЧАНИЕ',
    'Tovar Kat'=>'КАТЕГОРИЯ ПЕРЕДАЧИ',
    'Tove Kol'=>'КОЛИЧЕСТВО',
    'Tovar Desc'=>'ОПИСАНИЕ ПОСЫЛКИ',
    'Status'=>'СТАТУС',
    'Itog Price'=>'ИТОГОВАЯ СТОИМОСТЬ',
    'zakaz_dostavki_s:'=>'ВЫБЕРИТЕ НАПРАВЛЕНИЯ ПОСЫЛКИ',
    'zakaz_dostavki_to:'=>'ЗАПОЛНИТЕ ДАННЫЕ ПОЛУЧАТЕЛЯ ПОСЫЛКИ',
    'otdelenie_nova_poshta'=>'ВЫберите отделение новой почты',
    'Nomer doma'=>'Дом №',

    'o_posilke'=>'О ПОСЫЛКЕ',
    'Ocenka gruza'=>'Стоимость посылки',
    'gde_zabrat_parcel'=>'ЗАПОЛНИТЕ ДАННЫЕ ОТПРАВИТЕЛЯ ПОСЫЛКИ',

    'NAV_ZAKAZ_DRIVER_MY'=>'Мои принятые заказы',
    'NAV_ZAKAZ_DRIVER'=>'Ожидающие заказы',
    'DRIVER_VAWI_ZAKAZI'=>'Ваши принятые заказы',
    'prinyat_zakaz'=>'Подтвердите принятие заказа',
    'prinyat'=>'ПРИНЯТЬ',
    'otkaz'=>'ОТКАЗАТЬСЯ',

     'NAV_ZAKAZ_DRIVER_TOUK'=>'Ожидающие заказы в Англию',
    'NAV_ZAKAZ_DRIVER_TOUA'=>'Ожидающие заказы в Украину',


'Mest(sumok_korobok)'=>'Количество мест',


'Parcel Ves'=>'ПРИМЕРНЫЙ ВЕС ПОСЫЛКИ',
'V England'=>'В АНГЛИЮ',
'V Ukrainu'=>'В УКРАИНУ',


'Yes'=>'Да',
'No'=>'Нет',

'Active_orders_to_uk'=>'Активные заказы в Англию',
'Active_orders_to_ua'=>'Активные заказы в Украину',

'DRIVER'=>'Кабинет водителя',

'NAV_ZAKAZ_USER'=>'Мои заказы',

'Prosmotr_zakaza_#'=>'Просмотр заказа №',
'Zakazs'=>'Заказы',
'zakaz_kurera_s:'=>'Адрес отправителя',
'Zakaz_nomber_#'=>'Заказ №',
'Country'=>'Страна',
'Town'=>'Город',
'Post code'=>'Индекс',
'Nomer'=>'Дом №',
'Ulica'=>'Улица',
'Name'=>'ФИО',
'Telefon1'=>'Ном.тел1',
'Telefon2'=>'Ном.тел2',
'Mail'=>'Email',

'England'=>'Англия',
'Ukraine'=>'Украина',
'Dostavit posilku v:'=>'Адрес доставки',
'Vremya dostavki'=>'Время',
'Data zakaza'=>'Дата заказа',
'Ojidayut s'=>'Дата когда забрать',
'Ojidayut po'=>'Время',
'Posilka budet otpravlena vam na novuyu pushtu'=>'Посылка будет доставлена в отделение новой почты',
'prinyat'=>'Принять заказ',
'prinyat_zakaz?'=>'Принять заказ?',


'FLASH_OTKAZALIS_OT_ZAKAZA'=>'Вы отказались от заказа!',
'FLASH_ZAKAZ_PRINYAT'=>'Заказ принят и перемещен в "ЗАКАЗЫ В РАБОТЕ"',
'Active orders'=>'Принятые заказы',

'eng-ukr'=>'Англия - Украина',
'ukr-eng'=>'Украина - Англия',
'mejdunarodniy_format'=>'В международном формате. 12 цифр',
'tel_poluch_1'=>'Ном.тел1 получателя',
'tel_poluch_2'=>'Ном.тел2 получателя',
'to_firstname'=>'Имя получателя',
'to_lastname'=>'Фамилия получателя',
'next'=>'Далее',
'nazad'=>'Назад',

'nada_mest'=>'Всего мест',
'opisanie posilki'=>'Описание посылки',
'opisanie posilki_place'=>'Как можно подробнее опишите передаваемую посылку. Укажите все детали. ',
'Postcode or adress'=>'Индекс или адрес',
'House numbe'=>'Дом №',
'Street address'=>'Улица',
'from_firstname'=>'Имя отправителя',
'from_lastname'=>'Фамилия отправителя',
'from_mail'=>'E-mail отправителя',
'from_tel1'=>'Ном.тел1 отправителя',
'from_tel2'=>'Ном.тел2 отправителя',
'Select from time'=>'Забрать заказ с',
'Select to time'=>'Забрать заказ до',
'Create'=>'Создать заказ',
'House number'=>'Дом №',

'from_postcode'=>'Индекс',
'To Lastname'=>'Фамилия получателя',
'To Firstname'=>'Имя получателя',
'From Lastname'=>'Фамилия отправителя',
'From Firstname'=>'Имя отправителя',
'En Country'=>'Страна',
'En Postal Code'=>'Индекс',
'En City'=>'Город',
'En Street Name'=>'Дом №',
'En Street Number'=>'Улица',
'En Full Adress'=>'Индекс или адрес',
'From Time2'=>'Удобное время',
'From Time1'=>'Дата вызова курьера',
'From Tel2'=>'Ном.тел2 отправителя',
'From Tel1'=>'Ном.тел1 отправителя',
'Parcel Dlina'=>'Описание посылки',
'Parcel Wirina'=>'Новые вещи',
'Parcel Visota'=>'Количество мест',
'Parcel Ves'=>'Примерный вес(КГ)',
'To Tel2'=>'Телефон получателя2',
'Nova Poshta'=>'Новая почта',
'Itog Price'=>'Сумма в итоге',
'Status'=>'Статус',
'To Desc'=>'Описание',
'To City'=>'Город',
'To County'=>'Страна',
'from_mail'=>'E-mail отправителя',
'Name'=>'Имя получателя',
'Tel'=>'Ном.тел1 получателя',

'From Paradniy'=>'Дом №',
'From Street'=>'Улица',
'From City'=>'Город',
'From Country'=>'Страна',


'from_time1'=>'Дата вызова курьера',
'from_time2'=>'Удобное время',

'price'=>'Доставка и цены',
'onas'=>'О нас',
'rules'=>'Правила',

'oformlenie_zakaza_iz_uk_to_ua'=>'Оформление заказа на перевозку из Англии в Украину',

'new_ves'=>'Примерный вес новых вещей(КГ)',
'Novie_veshi'=>'Имеются новые вещи?(новый товар приобритенный в магазинах)',
'cena'=>'Во сколько вы оцениваете новые вещи?(£)',
'gabarit'=>'Имеются габаритные вещи?(диван, плита, холодильник)',
'image'=>'Фото габаритной вещи1',
'image2'=>'Фото габаритной вещи2',
'gabarit_podskazka'=>'Подсказка',
'Novie_veshi_podskazka'=>'Подсказка',


'cena_posilki_podskazka'=>'Без учета габаритных вещей.',
'kol_sumok_paketov'=>'Всего отдельных посылок(1 сумка + 2 пакета + холодильник = 4)',
'ves_posilki'=>'Общий вес посылки(КГ)',

'VAW_ZAKAZ_PRINYAT'=>'Ваш заказ принят! Ожидайте SMS подтверждения!',

'Ves posilki'=>'Общий вес посылки, КГ',
'Opisanie_posilki'=>'Описание посылки',
'Gabaritnie_veshi'=>'Есть габаритные вещи?',



/*'text_rules'=>' <h3>Как это работает</h3>
    <p>При доставке из Англии в Украину:
    <ul>
    <li>Создаете заказ на доставку
    <li>Получаете смс подтверждение
    <li>В указаное время приезжает курьер и забирает посылку
    <li>Посылку привозят в Украину и отправляют "новой почтой" на указаный вами адрес
    </ul>',*/
    

    
  
];

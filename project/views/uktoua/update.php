<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UkToUa */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Uk To Ua',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Uk To Uas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="uk-to-ua-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

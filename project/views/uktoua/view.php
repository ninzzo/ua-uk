<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\UkToUa */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Uk To Uas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="uk-to-ua-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'from_fio',
            'from_adress',
            'from_tel',
            'from_gruz:ntext',
            'from_kod',
            'to_city',
            'to_pochta',
            'to_tel',
            'to_fio',
            'when',
            'track',
        ],
    ]) ?>

</div>

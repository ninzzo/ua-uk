<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UkToUaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="uk-to-ua-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'from_fio') ?>

    <?= $form->field($model, 'from_adress') ?>

    <?= $form->field($model, 'from_tel') ?>

    <?= $form->field($model, 'from_gruz') ?>

    <?php // echo $form->field($model, 'from_kod') ?>

    <?php // echo $form->field($model, 'to_city') ?>

    <?php // echo $form->field($model, 'to_pochta') ?>

    <?php // echo $form->field($model, 'to_tel') ?>

    <?php // echo $form->field($model, 'to_fio') ?>

    <?php // echo $form->field($model, 'when') ?>

    <?php // echo $form->field($model, 'track') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

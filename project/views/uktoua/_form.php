<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UkToUa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="uk-to-ua-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'from_fio')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'from_adress')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'from_tel')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'from_gruz')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'from_kod')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'to_city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'to_pochta')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'to_tel')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'to_fio')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'when')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'track')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

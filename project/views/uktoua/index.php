<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UkToUaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Uk To Uas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="uk-to-ua-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Uk To Ua'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'from_fio',
            'from_adress',
            'from_tel',
            'from_gruz:ntext',
            // 'from_kod',
            // 'to_city',
            // 'to_pochta',
            // 'to_tel',
            // 'to_fio',
            // 'when',
            // 'track',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

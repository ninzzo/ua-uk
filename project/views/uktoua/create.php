<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\date\DatePicker;
use yii\helpers\Url;

use yii\widgets\MaskedInput;
//use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use kartik\file\FileInput;


/* @var $this yii\web\View */
/* @var $model app\models\UkToUa */

//$this->title = Yii::t('app', 'Create Uk To Ua');

$this->title = Yii::t('app', 'Create Uk To Ua').' | '.Yii::$app->name;
?>


<div class="wrap-body">
       

<div class="container">
                            <div class="row">
                             <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                             </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <div class="modal-content">

 

                                        <div class="modal-header">                                            
                                            <p class="modal-title lead text-center zagl">Заказ отправки из Англии в Украину</p>
                                            <p class="text-center zagl2">Для оформления заказа заполните форму</p>
                                        </div>
                                        <div class="modal-body">


<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>  
<div class="form-group ">
 		<?php                 
        echo $form->field($model, 'to_city')->widget(Select2::classname(), [
            'data' => [
                '1' => 'Авангард',        
                ],
            'language' => 'en',
            'options' => ['id'=>'city', 'data-toggle'=>'tooltip', 'title'=>"Город куда вы хотите отправить посылку", 'data-placement'=>"left"],
            'pluginOptions' => [
                'allowClear' => false,
            ],
        ]);
              ?>
 </div>

        <?= $form->field($model, 'to_pochta')->dropDownList([
              'Отделение № 1: ул. Абрикосовая, 1' => 'Отделение № 1: ул. Абрикосовая, 1',      
              ], ['id'=>'nova_poshta']);

        ?>

        <?= $form->field($model, 'to_fio')->textInput(); ?>

        <?= $form->field($model, 'to_tel')->textInput(); ?>
        <hr>

        <?= $form->field($model, 'from_fio')->textInput(); ?>
            

        <div class="row">
            <div class="col-sm-4">                 
                <?php 
$kod_tel = array('+4 (4753)'=>'+4 (4753)','+4 (4758)'=>'+4 (4758)','+4 (4751)'=>'+4 (4751)','+4 (6658)'=>'+4 (6658)',);
            echo $form->field($model, 'from_kod')->dropDownList([$kod_tel]); ?>
            </div>
            <div class="col-sm-8">
                <?= $form->field($model, 'from_tel')->textInput(); ?>
			</div>
        </div>

        <?= $form->field($model, 'from_adress')->textInput(); ?>  

        <?= $form->field($model, 'when')->textInput(); ?>   

        <?= $form->field($model, 'from_gruz')->textInput(); ?>   
      
                                           
                                                

                                                

                                        </div>
                                        <div class="modal-footer" style="text-align: center;">

                                <?= Html::submitButton('Оформить заказ', ['class' => 'btn btn-primary']) ?>
                                          
                                        </div>  
                                        <?php ActiveForm::end(); ?>                                      
                                    </div>                                    
                                </div>
                            </div>
                        </div>



    </div>

<script>

function country(n){

   $.ajax({
    type     :'POST',
    cache    : false,
    url  : "<?php echo Url::toRoute(['/main/default/getcity']);?>",
    dataType: 'html',
    data: {country_id: n},
    success  : function(response) {     
     $('#city').html(response);
   }
 });
 }

 function city(n){

   $.ajax({
    type     :'POST',
    cache    : false,
    url  : "<?php echo Url::toRoute(['/main/default/getotdeli']);?>",  
    dataType: 'html',
    data: {city_id: n},
    success  : function(response) {  
     $('#nova_poshta').html(response);
   }
 });
 }
 $(document).ready(function () {

$( "#city" ).change(function() {    
      city($("select#city").val());   
  });

country(2);

});
</script>
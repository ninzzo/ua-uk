<?php

use yii\helpers\Html;
use app\assets\AppAsset1;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset1::register($this);
$title = 'UA-UK. Сервис доставки товаров и посылок из Англии в Украину и обратно';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta name="yandex-verification" content="9ebe53a86f64e597" />
    <meta name="google-site-verification" content="ZlZd8PeUK7oQmxkWeLES39Z7lN90RR5rNL9zHvD0nWY" />
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title.' | '.$title) ?></title>
    <?php $this->head() ?>
   
</head>
<body>

<?php $this->beginBody() ?>


<?= $this->render('_main_menu') ?>
<div class="wrapper">
        <?= $content ?>
<?= $this->render('_main_footer') ?>
</div>    

<?php $this->endBody() ?>


<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'Nt7Nnw2eAa';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->
</body>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-78715763-1', 'auto');
  ga('send', 'pageview');

</script>

</html>
<?php $this->endPage() ?>

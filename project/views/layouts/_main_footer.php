<?php
use yii\helpers\Html;
?>

<div class="wrap-footer">
        <div class="container">
            <div class="row footer-block">
                <div class="col-lg-2 col-sm-4 col-md-2 col-xs-12 ">
                    <div class="footer-div"><?= Html::a('О компании', ['/main/default/about'],['class'=>'footer-color']) ?>
                    </div>
                    <div class="footer-div"><?= Html::a('Доставка и цены', ['/main/default/howmuch'],['class'=>'footer-color']) ?></div>
                    <!--<div class="footer-div"><a href="#" class="footer-color">Новости</a></div>-->
                </div>
                <div class="col-lg-2 col-sm-4 col-md-2 col-xs-12 ">
                    <!--<div class="footer-div"><a href="#" class="footer-color">Статьи</a></div>-->
                    <div class="footer-div"><?= Html::a('Почему мы?', ['/main/default/about'],['class'=>'footer-color']) ?></a></div>
                    <div class="footer-div "><?= Html::a('Как это работает?', ['/main/default/rules'],['class'=>'footer-color']) ?></a></div>
                </div>
                <div class="col-lg-2 col-sm-4 col-md-2 col-xs-12 ">
                    <!--<div class="footer-div"><a href="#" class="footer-color"><i
                            class="glyphicon glyphicon-search icon-blue"></i> Отследить посылку </a></div>-->
                    <div class="footer-div"><a href="#" class="footer-color"><i class="fa fa-calculator"></i>
                        Расчитать
                        стоимость</a></div>
                    <div class="footer-div ">
<?= Html::a('<i class="glyphicon glyphicon-envelope icon-blue"></i> Связаться с нами', ['/main/contact/index'],['class'=>'footer-color']) ?>

                            </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-md-3 col-xs-12">
                    <p class="footer-chapter text-center text-uppercase lead ">Звоните прямо сейчас</p>
                    <address>
                        <div class="number "><span class="England">  Англия &nbsp </span> <img src="<?php echo Yii::getAlias('@web'); ?>/image/eng.png" alt="eng"
                                                                                        class="lang-small">
                            <span class="firstP-number"> +4 (4758)</span><span class="secondPNumber"> 811-2834</span>
                        </div>
                        <div class="number"><span class="England"> Украина </span> <img src="<?php echo Yii::getAlias('@web'); ?>/image/ukr.png" alt="ukr" class="lang-small">
                            <span class="firstP-number  "> +3 (8063)</span><span
                                    class="secondPNumber"> 756-0299</span>
                        </div>
                    </address>
                </div>
                <div class="col-lg-3 col-sm-6 col-md-3 col-xs-12 ">
                    <p class="lead text-center text-uppercase footer-chapter"> Мы в соц сетях </p>


                    <div class="col-lg-3 col-sm-3 col-md-3 col-xs-3 ">
                        <div class="FTV">
                            <a href="https://vk.com/club137838866" target="_blank">
                                <img src="<?php echo Yii::getAlias('@web'); ?>/image/vk.png" class="img-responsive" alt="vk">
                            </a>
                        </div>
                    </div>
                    <!--<div class="col-lg-3 col-sm-3 col-md-3 col-xs-3 ">
                        <div class="FTV">
                            <a href="#">
                                <img src="<?php echo Yii::getAlias('@web'); ?>/image/twitter.png" class="img-responsive" alt="twitter">
                            </a>
                        </div>
                    </div>-->
                    <div class="col-lg-3 col-sm-3 col-md-3 col-xs-3 ">
                        <div class="FTV">
                            <a href="https://www.facebook.com/uaukdelivery" target="_blank">
                                <img src="<?php echo Yii::getAlias('@web'); ?>/image/face.png" class="img-responsive" alt="face">
                            </a>
                        </div>
                    </div>
                    <!--<div class="col-lg-3 col-sm-3 col-md-3 col-xs-3 ">
                        <div class="FTV">
                            <a href="#">
                                <img src="<?php echo Yii::getAlias('@web'); ?>/image/twitt.png" class="img-responsive" alt="twitt">
                            </a>
                        </div>
                    </div>-->
                </div>
            </div>
        </div>
    </div>

    <div class="wrap-down ">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                    <p class=" text-center"> © UA-UK 2017 Сервис доставки товаров и посылок из Англии в Украину и
                        обратно</p>
                </div>
            </div>
        </div>
    </div>
<?php
use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<nav id="w0" class="navbar navbar-inverse " role="navigation">

    <div class="container">
        <div class="row">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#w0-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <div class="col-xs-9 col-sm-12 col-md-12 col-lg-7">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-3  ">
                        <div class="language-picker button-list">
                            <div class="icon-lang">
                                <?= \lajax\languagepicker\widgets\LanguagePicker::widget([
    'skin' => \lajax\languagepicker\widgets\LanguagePicker::SKIN_BUTTON,
    'size' => \lajax\languagepicker\widgets\LanguagePicker::SIZE_LARGE
]); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-9  ">



                        <div id="w0-collapse" class="collapse navbar-collapse">
<?php echo Nav::widget([
    'options' => ['class' => 'navbar-nav'],
    'activateParents' => true,
    'encodeLabels' => false,
    'items' => array_filter([
        ['label' => Yii::t('app', 'NAV_HOME'), 'url' => ['/main/default/index']],
        ['label' => Yii::t('app', 'Как это работает'), 'url' => ['/main/default/rules']],

    ]),
]);?>


                         <!--   <ul id="w1" class="nav navbar-nav ">
                                <li><a href="#">О компании</a></li>
                                <li class="active"><a href="/main/default/price">Доставка и цены</a></li>
                                <li><a href="/main/default/onas">Отзивы</a></li>
                                <li><a href="/login">Новости</a></li>
                            </ul>-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix visible-sm-block visible-xs-block"></div>
            <div class="col-xs-8 col-sm-8 col-md-4 ">
                <form class="navbar-form" method="POST" role="search" action="<?= Url::toRoute('/track/index') ?>">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Введите трекинг номер" name="srch-term"
                               id="srch-term" required>

                        <div class="input-group-btn">
                            <button class="btn  btn-primary img-rounded icon-search" type="submit">
                                Отследить
                            </button>
                        </div>
                    </div>
                </form>
            </div>



            <!--<div class="col-xs-4 col-sm-3 col-md-1 ">
                <button class="btn btn-info enter" type="button" data-toggle="modal" data-target="#myModal">Вход
                </button>
                <div id="myModal" class="modal fade">
                    <div class="modal-dialog">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-11 col-sm-10 col-md-8 col-lg-6">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button class="close" type="button" data-dismiss="modal">×</button>
                                            <p class="modal-title lead text-center zagl">Войти на сайт</p>

                                            <p class="text-center zagl2">Для входа на сайт заведите данные своей учетной
                                                записи</p>
                                        </div>
                                        <div class="modal-body">
                                            <form method="post" action="index.html">
                                                <div class="form-group">
                                                    <div class="col-xs-11 col-sm-11 col-md-11 col-lg-11">
                                                        <p><input type="text" class="modaText form-control" name="login"
                                                                  value=""
                                                                  placeholder="Логин (Email)"></p>
                                                    </div>
                                                </div>
                                                <div class="col-xs-11 col-sm-11 col-md-11 col-lg-11">
                                                    <div class="form-group">
                                                        <p><input type="password" class="modaPassword form-control"
                                                                  name="password" value=""
                                                                  placeholder="Пароль"></p>
                                                    </div>
                                                </div>
                                                <p class="remember_me text-center ">
                                                    <label>
                                                        <input type="checkbox" name="remember_me" id="remember_me">
                                                        Запомнить меня
                                                    </label>
                                                </p>

                                                <div class="login-help">
                                                    <a href="#" type="button" data-toggle="modal"
                                                       data-target="#myRegist">Регистрация &nbsp</a>


                                                    <a href="#" type="button" data-toggle="modal"
                                                       data-target="#myRepass">&nbsp Востановить пароль?</a>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-link center-block join" type="button"
                                                    data-dismiss="modal">
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="myRegist" class="modal fade">
                    <div class="modal-dialog">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-11 col-sm-10 col-md-8 col-lg-6">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <button class="close" type="button"
                                                    data-dismiss="modal">×
                                            </button>

                                            <p class="modal-title lead text-center zagl">
                                                Регистрация</p>

                                            <p class="text-center zagl2">Для
                                                регистрации заполните следующие
                                                поля</p>

                                            <div class="form-group ">
                                                <label class="control-label"
                                                       for="inputEmail"></label>

                                                <div class="">
                                                    <input type="email"
                                                           class="form-control"
                                                           id="inputEmail"
                                                           placeholder="Логин (E-mail)">
                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <label class="control-label"
                                                       for="lastName"></label>

                                                <div class="">
                                                    <input type="text"
                                                           class=" form-control"
                                                           id="lastName"
                                                           placeholder="Фамилию">
                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <label class="control-label"
                                                       for="firstName"></label>

                                                <div class="">
                                                    <input type="text"
                                                           class="form-control "
                                                           id="firstName"
                                                           placeholder="Имя">
                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <label class="control-label"
                                                       for="inputPassword"></label>

                                                <div class="">
                                                    <input type="password"
                                                           class="form-control"
                                                           id="inputPassword"
                                                           placeholder="Пароль">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label"
                                                       for="phoneNumber"></label>

                                                <div class="">
                                                    <input type="tel"
                                                           class="form-control"
                                                           id="phoneNumber"
                                                           placeholder="Номер телефона">
                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <button class="btn btn-link center-block regist"
                                                        type="button"
                                                        data-dismiss="modal">

                                                </button>

                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div id="myRepass" class="modal fade">
                    <div class="modal-dialog">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-11 col-sm-10 col-md-8 col-lg-6">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <button class="close" type="button" data-dismiss="modal">×</button>
                                            <p class="modal-title lead text-center zagl">Востановить пароль</p>

                                            <p class="text-center zagl2">Введите свой Email и мы пришлем вам инструкцию
                                                по востановлению</p>

                                            <form method="post" action="index.html">
                                                <div class="form-group">
                                                    <div class="col-xs-11 col-sm-11 col-md-11 col-lg-11">
                                                        <p><input type="text" class="modaText form-control" name="login"
                                                                  value=""
                                                                  placeholder="Логин (Email)"></p>
                                                    </div>
                                                </div>
                                            </form>

                                            <button class="btn btn-link  center-block  send" type="button"
                                                    data-dismiss="modal">
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
        </div>
    </div>
    </div>
</nav>

<div class="wrap-menu">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12 fon">
                    <div class="col-lg-4 col-md-4 col-sm-11 col-xs-11  ">
                        <img src="<?php echo Yii::getAlias('@web'); ?>/image/UA.png" alt="UA" class="UK img-responsive">
                    </div>
                    <div class="col-lg-5  col-md-4 col-sm-7 col-xs-12 ">
                        <div class="col-lg-5 col-md-6 col-sm-5 col-xs-5 ">                            
                            <?= Html::a('', ['/main/default/about'],['class'=>'btn btn-link whyWe']) ?>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">                            
                            <?= Html::a('', ['/main/default/howmuch'],['class'=>'btn btn-link howMuch']) ?>
                        </div>
                    </div>


                    <div class="col-lg-3  col-md-4 col-sm-5 col-xs-10 pull-right ">
                        <address>
                            <div class="number"><span class="Ukraine"> Англия  &nbsp; </span> <img src="<?php echo Yii::getAlias('@web'); ?>/image/eng.png"
                                                                                           alt="eng"
                                                                                           class="lang-small">
                                <span class="ukrFirst"> +4 (4758)</span><span class="ukrSecond"> 811-2834</span>
                            </div>

                            <div class="number"><span class="Ukraine"> Украина </span> <img src="<?php echo Yii::getAlias('@web'); ?>/image/ukr.png"
                                                                                            alt="ukr"
                                                                                            class="lang-small">
                                <span class="ukrFirst"> +3 (8063)</span><span class="ukrSecond"> 756-0299</span>
                            </div>
                        </address>
                    </div>

                </div>
            </div>
        </div>
    </div>


  
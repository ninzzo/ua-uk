<?php

use app\components\widgets\Alert;
use app\modules\admin\rbac\Rbac as AdminRbac;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use app\modules\zakaz\models\Zakaz;

/* @var $this \yii\web\View */
/* @var $content string */

?>
<?php $this->beginContent('@app/views/layouts/layout.php'); ?>

<?php
$zakaz_all = Zakaz::find()->where(['status'=>0])->all();
count($zakaz_all) > 0 ? $badge1=Yii::t('app', 'NAV_ZAKAZ_DRIVER').Html::tag('span', count($zakaz_all), ['class' => 'label label-danger label-as-badge']) : $badge1=Yii::t('app', 'NAV_ZAKAZ_DRIVER');

$id = Yii::$app->user->id;

$zakaz_my = Zakaz::find()->where(['status'=>1, 'driver_id'=>$id])->all();
$zakaz_my=count($zakaz_my) > 0 ? $badge2=Yii::t('app', 'NAV_ZAKAZ_DRIVER_MY').Html::tag('span', count($zakaz_my), ['class' => 'label label-success label-as-badge']) : $badge2=Yii::t('app', 'NAV_ZAKAZ_DRIVER_MY');




NavBar::begin([
    'brandLabel' => Yii::$app->name,    
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar-inverse navbar-fixed-top',
    ],
]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'activateParents' => true,
    'encodeLabels' => false,
    'items' => array_filter([
        ['label' => Yii::t('app', 'NAV_HOME'), 'url' => ['/main/default/index']],
        //['label' => Yii::t('app', 'rules'), 'url' => ['/main/default/rules']],

        /*!Yii::$app->user->can('driver') ?
            ['label' => Yii::t('app', 'price'), 'url' => ['/main/default/price']] :
            false,

        !Yii::$app->user->can('driver') ?
            ['label' => Yii::t('app', 'onas'), 'url' => ['/main/default/onas']] :
            false,*/

            !Yii::$app->user->can('admin') ?
            ['label' => Yii::t('app', 'price'), 'url' => ['/main/default/price']] :
            false,

        !Yii::$app->user->can('admin') ?
            ['label' => Yii::t('app', 'onas'), 'url' => ['/main/default/onas']] :
            false,
      


        /*Yii::$app->user->isGuest ?
            ['label' => Yii::t('app', 'NAV_SIGNUP'), 'url' => ['/user/default/signup']] :
            false,*/
        Yii::$app->user->isGuest ?
            ['label' => Yii::t('app', 'NAV_LOGIN'), 'url' => ['/user/default/login']] :
            false,
        /*Yii::$app->user->can(AdminRbac::PERMISSION_ADMIN_PANEL) ?
            ['label' => Yii::t('app', 'NAV_ADMIN'), 'url' => ['/admin/default/index']] :
            false,*/
            Yii::$app->user->can('admin') ?
            ['label' => Yii::t('app', 'ADMIN_UPRAVLENIE'), 'url' => ['/admin/users/default/index']] :
            false,
            /*!Yii::$app->user->can(AdminRbac::PERMISSION_ADMIN_PANEL) ?
            ['label' => Yii::t('app', 'NAV_ZAKAZ_USER'), 'url' => ['/user/profile/myzakaz']] :
            false,
            Yii::$app->user->can('admin') ?            
                 ['label' => $badge1, 'items' => [
                ['label' => Yii::t('app', 'NAV_ZAKAZ_DRIVER_TOUK'), 'url' => ['/driver/driver/touk']],
                ['label' => Yii::t('app', 'NAV_ZAKAZ_DRIVER_TOUA'), 'url' => ['/driver/driver/toua'],
            ]]] :
            false,*/
        !Yii::$app->user->isGuest ?            
            ['label' => Yii::t('app', 'NAV_LOGOUT'), 'url' => ['/user/default/logout']] :
            false,
    ]),
]);?>


<div class="col-sm-3 col-md-3 pull-right">
        <form class="navbar-form" role="search">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Отследить посылку" name="srch-term" id="srch-term">
            <div class="input-group-btn">
                <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
            </div>
        </div>
        </form>
        </div>

<div class="navbar-text pull-left">
    <?= \lajax\languagepicker\widgets\LanguagePicker::widget([
    'skin' => \lajax\languagepicker\widgets\LanguagePicker::SKIN_BUTTON,
    'size' => \lajax\languagepicker\widgets\LanguagePicker::SIZE_LARGE
]); ?>
</div>
<?php
NavBar::end();
?>

<div class="container">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <?= Alert::widget() ?>
    <?= $content ?>
</div>
<!--background: url('https://www.warbble.com/assets/front/img/bg_contactus.png') no-repeat;-->
<style>


.label-as-badge {
    border-radius: 1em;
    margin-left: 5px;
}
    body{
        
        
        background: #f1f1f1;
        background-size: cover;
       
    }

    

</style>

<?php $this->endContent(); ?>

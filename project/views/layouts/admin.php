<?php

use app\components\widgets\Alert;
use app\modules\admin\Module;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use app\modules\zakaz\models\Zakaz;

/* @var $this \yii\web\View */
/* @var $content string */

/** @var \yii\web\Controller $context */
$context = $this->context;

if (isset($this->params['breadcrumbs'])) {
    $panelBreadcrumbs = [['label' => Module::t('module', 'ADMIN'), 'url' => ['/admin/default/index']]];
    $breadcrumbs = $this->params['breadcrumbs'];
} else {
    $panelBreadcrumbs = [Module::t('module', 'ADMIN')];
    $breadcrumbs = [];
}
?>
<?php $this->beginContent('@app/views/layouts/layout.php'); ?>

<style>
.label-as-badge {
    border-radius: 1em;
    margin-left: 5px;
}
</style>

<?php
$zakaz_all = Zakaz::find()->where(['status'=>0])->all();
count($zakaz_all) > 0 ? $badge1=Yii::t('app', 'NAV_ZAKAZ_ADMIN').Html::tag('span', count($zakaz_all), ['class' => 'label label-danger label-as-badge']) : $badge1=Yii::t('app', 'NAV_ZAKAZ_ADMIN');

$zakaz_my = Zakaz::find()->where(['status'=>1, 'driver_id'=>Yii::$app->user->id])->all();
$zakaz_my=count($zakaz_my) > 0 ? $badge2=Yii::t('app', 'NAV_ZAKAZ_ADMIN_MY').Html::tag('span', count($zakaz_my), ['class' => 'label label-success label-as-badge']) : $badge2=Yii::t('app', 'NAV_ZAKAZ_ADMIN_MY');




NavBar::begin([
    'brandLabel' => Yii::$app->name,
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar-inverse navbar-fixed-top',
    ],
]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'activateParents' => true,
    'encodeLabels' => false,
    'items' => array_filter([
        //['label' => Yii::t('app', 'NAV_HOME'), 'url' => ['/main/default/index']],
        ['label' => $badge2, 'url' => ['/driver/driver/myzakaz']],
        ['label' => $badge1, 'items' => [
                ['label' => Yii::t('app', 'NAV_ZAKAZ_ADMIN_TOUK'), 'url' => ['/driver/driver/touk']],
                ['label' => Yii::t('app', 'NAV_ZAKAZ_ADMIN_TOUA'), 'url' => ['/driver/driver/toua'],
            ]]],
        
      
        ['label' => Yii::t('app', 'ADMIN_USERS'), 'url' => ['/admin/users/default/index'], 'active' => $context->module->id == 'users'],
        ['label' => Yii::t('app', 'NAV_LOGOUT'), 'url' => ['/user/default/logout'], 'linkOptions' => ['data-method' => 'post']]
    ]),
]);
NavBar::end();
?>

<div class="container">
    <?= Breadcrumbs::widget([
        'links' => ArrayHelper::merge($panelBreadcrumbs, $breadcrumbs),
    ]) ?>
    <?= Alert::widget() ?>
    <?= $content ?>
</div>

<?php $this->endContent(); ?>
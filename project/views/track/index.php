<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
$this->title = 'Отследить посылку';
?>

<div class="wrap-body">
        <div class="container">
            <div class="row">
                <div class=" col-md-12 col-sm-12 col-xs-12 ">
                <h3>Отследить посылку</h3>

<?php if( isset($_POST['srch-term']) ) { ?>
                <p>
                 <ul>
                <?php

                

                foreach( $model as $m ) {
                	if($m->status == 0){ echo '<li>'.date("d.m.y H:i", $m->date).' Заказ создан и ожидает подтверждения оператором.</li>'; }
                	if($m->status == 1){ echo '<li>'.date("d.m.y H:i", $m->date).' Заказ подтвержден. Ожидайте курьера по указаному вами адресу.</li>'; }
                	if($m->status == 2){ echo '<li>'.date("d.m.y H:i", $m->date).' Посылка в пути из Англии в Украину.</li>'; }
                	if($m->status == 3){ echo '<li>'.date("d.m.y H:i", $m->date).' Посылка доставлена на новую почту в Украине. Трекинг код новой почты - NNNNNN. Отследить доставку можно <a target="_blanck" href="https://novaposhta.ua/tracking">здесь</a></li>'; }
                	

                	
                
            	}

                ?>
                </ul>

<?php } else { ?>

 <form class="navbar-form" method="POST" role="search" action="<?= Url::toRoute('/track/index') ?>">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Введите трекинг номер" name="srch-term"
                               id="srch-term" required>

                        <div class="input-group-btn">
                            <button class="btn  btn-primary img-rounded icon-search" type="submit">
                                Отследить
                            </button>
                        </div>
                    </div>
 </form>

 <?php } ?>





               
                </div>
            </div>
        </div>
    </div>

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

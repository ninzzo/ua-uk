<?php
$config = [
    'id' => 'app',
    //'language'=>$_SESSION['lan'],

    'language' => 'en',
    'bootstrap' => ['languagepicker'],
    



    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\Module',
            'layout' => '@app/views/layouts/admin',
            'modules' => [
                'users' => [
                    'class' => 'app\modules\user\Module',
                    'controllerNamespace' => 'app\modules\user\controllers\backend',
                    'viewPath' => '@app/modules/user/views/backend',
                ],
            ]
        ],
        'main' => [
            'class' => 'app\modules\main\Module',
            'layout' => '@app/views/layouts/main_page',
        ],
        'user' => [
            'class' => 'app\modules\user\Module',
            'controllerNamespace' => 'app\modules\user\controllers\frontend',
            'viewPath' => '@app/modules/user/views/frontend',
        ],
        'zakaz' => [
            'class' => 'app\modules\zakaz\Module',
            'layout' => '@app/views/layouts/admin',
        ],
        'driver' => [
            'class' => 'app\modules\driver\Module',
            'layout' => '@app/views/layouts/admin',
        ],
       
        
    ],
    'components' => [


        'languagepicker' => [
        'class' => 'lajax\languagepicker\Component',
        'languages' => ['en', 'ru', 'uk']                   // List of available languages (icons only)
        ],


        'user' => [
            'identityClass' => 'app\components\UserIdentity',
            'enableAutoLogin' => true,
            'loginUrl' => ['user/default/login'],
        ],
        'errorHandler' => [
            'errorAction' => 'main/default/error',
        ],
        'request' => [
            'cookieValidationKey' => '',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
            'enableCsrfValidation'=>false,
            'enableCookieValidation'=>false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
        ],
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    //$config['modules']['gii'] = 'yii\gii\Module';
    $config['modules']['gii'] = ['class' => \yii\gii\Module::className(),
    'allowedIPs' => ['*']];
}

return $config;

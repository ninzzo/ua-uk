<?php

namespace app\controllers;
use app\models\Track;

class TrackController extends \yii\web\Controller
{
	 public $layout = 'main_page';
    public function actionIndex()
    {
    	
    	if(isset($_POST['srch-term'])){
    		$result = Track::find()
		     ->where(['track' => $_POST['srch-term']])
		     ->orderBy(['date' => SORT_ASC])
		     ->all();

		    return $this->render('index', [
                'model' => $result,
            ]);
    	} else {
    		return $this->render('index');
    	}
          
    }
    

}

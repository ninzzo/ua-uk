<?php

namespace app\models;

use Yii;
use app\models\Track;

/**
 * This is the model class for table "UkToUa".
 *
 * @property integer $id
 * @property string $from_fio
 * @property string $from_adress
 * @property string $from_tel
 * @property string $from_gruz
 * @property string $from_kod
 * @property string $to_city
 * @property string $to_pochta
 * @property string $to_tel
 * @property string $to_fio
 * @property string $when
 * @property string $track
 */
class UkToUa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'UkToUa';
    }


    public function beforeSave()
    {  
     if($this->isNewRecord)
     {
      $code = substr(md5(time()), 0, 10);
      $this->track = $code;   
      $tr = new Track();
      $tr->track = $code;
      $tr->date = time();
      $tr->status = 0;
      $tr->save(false);

     }     
     return TRUE;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_fio', 'from_adress', 'from_tel', 'from_gruz', 'from_kod', 'to_city', 'to_pochta', 'to_tel', 'to_fio', 'when',], 'required'],
            [['from_gruz'], 'string'],
            [['from_fio', 'from_adress', 'from_tel', 'to_city', 'to_pochta', 'to_tel', 'to_fio', 'when'], 'string', 'max' => 255],
            [['from_kod'], 'string', 'max' => 30],
            [['track'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'from_fio' => 'Ваша фамилия и имя(отправителя)',
            'from_adress' => 'Укажите точный адрес где забирать посылку. Индекс, Город, Улица, Дом. По этому адресу к вам приедет курьер',           
            'from_gruz' => 'Опишите посылку. Что вы передаете ? Примерный вес?',
            'from_kod' => 'Ваш код оператора',
             'from_tel' => 'Ваш номер телефона',
            'to_city' => 'В какой город Украины отправляете',
            'to_pochta' => 'Отделение новой почты в выбраном городе куда отправляете посылку',
            'to_tel' => 'Телефон получателя который заберет посылку на новой почте в Украине',
            'to_fio' => 'Фамилия и имя получателя который заберет посылку на почте в Украине',
            'when' => 'Дата и время когда к вам подъедет курьер что бы забрать посылку в Англии',
            'track' => Yii::t('app', 'Track'),
        ];
    }
}

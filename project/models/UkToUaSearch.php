<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UkToUa;

/**
 * UkToUaSearch represents the model behind the search form about `app\models\UkToUa`.
 */
class UkToUaSearch extends UkToUa
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['from_fio', 'from_adress', 'from_tel', 'from_gruz', 'from_kod', 'to_city', 'to_pochta', 'to_tel', 'to_fio', 'when', 'track'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UkToUa::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'from_fio', $this->from_fio])
            ->andFilterWhere(['like', 'from_adress', $this->from_adress])
            ->andFilterWhere(['like', 'from_tel', $this->from_tel])
            ->andFilterWhere(['like', 'from_gruz', $this->from_gruz])
            ->andFilterWhere(['like', 'from_kod', $this->from_kod])
            ->andFilterWhere(['like', 'to_city', $this->to_city])
            ->andFilterWhere(['like', 'to_pochta', $this->to_pochta])
            ->andFilterWhere(['like', 'to_tel', $this->to_tel])
            ->andFilterWhere(['like', 'to_fio', $this->to_fio])
            ->andFilterWhere(['like', 'when', $this->when])
            ->andFilterWhere(['like', 'track', $this->track]);

        return $dataProvider;
    }
}

<?php

namespace app\modules\driver\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\zakaz\models\Zakaz;

/**
 * DriverSearch represents the model behind the search form about `app\modules\zakaz\models\Zakaz`.
 */
class DriverSearch extends Zakaz
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'driver_id', 'created_at', 'updated_at', 'plus_money', 'status', 'itog_price', 'parcel_ves', 'parcel_visota', 'parcel_wirina', 'parcel_dlina'], 'integer'],
            [['from_country', 'from_city', 'from_street', 'from_paradniy', 'from_desc', 'tel', 'name', 'to_county', 'to_city', 'to_desc', 'nova_poshta', 'to_tel2', 'from_tel1', 'from_tel2', 'from_time1', 'from_time2', 'en_full_adress', 'en_street_number', 'en_street_name', 'en_city', 'en_postal_code', 'en_country', 'from_firstname', 'from_lastname', 'to_firstname', 'to_lastname'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Zakaz::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'driver_id' => $this->driver_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'plus_money' => $this->plus_money,
            'status' => $this->status,
            'itog_price' => $this->itog_price,
            'parcel_ves' => $this->parcel_ves,
            'parcel_visota' => $this->parcel_visota,
            'parcel_wirina' => $this->parcel_wirina,
            'parcel_dlina' => $this->parcel_dlina,
        ]);

        $query->andFilterWhere(['like', 'from_country', $this->from_country])
            ->andFilterWhere(['like', 'from_city', $this->from_city])
            ->andFilterWhere(['like', 'from_street', $this->from_street])
            ->andFilterWhere(['like', 'from_paradniy', $this->from_paradniy])
            ->andFilterWhere(['like', 'from_desc', $this->from_desc])
            ->andFilterWhere(['like', 'tel', $this->tel])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'to_county', $this->to_county])
            ->andFilterWhere(['like', 'to_city', $this->to_city])
            ->andFilterWhere(['like', 'to_desc', $this->to_desc])
            ->andFilterWhere(['like', 'nova_poshta', $this->nova_poshta])
            ->andFilterWhere(['like', 'to_tel2', $this->to_tel2])
            ->andFilterWhere(['like', 'from_tel1', $this->from_tel1])
            ->andFilterWhere(['like', 'from_tel2', $this->from_tel2])
            ->andFilterWhere(['like', 'from_time1', $this->from_time1])
            ->andFilterWhere(['like', 'from_time2', $this->from_time2])
            ->andFilterWhere(['like', 'en_full_adress', $this->en_full_adress])
            ->andFilterWhere(['like', 'en_street_number', $this->en_street_number])
            ->andFilterWhere(['like', 'en_street_name', $this->en_street_name])
            ->andFilterWhere(['like', 'en_city', $this->en_city])
            ->andFilterWhere(['like', 'en_postal_code', $this->en_postal_code])
            ->andFilterWhere(['like', 'en_country', $this->en_country])
            ->andFilterWhere(['like', 'from_firstname', $this->from_firstname])
            ->andFilterWhere(['like', 'from_lastname', $this->from_lastname])
            ->andFilterWhere(['like', 'to_firstname', $this->to_firstname])
            ->andFilterWhere(['like', 'to_lastname', $this->to_lastname]);

        return $dataProvider;
    }
}

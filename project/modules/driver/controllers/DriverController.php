<?php

namespace app\modules\driver\controllers;

use Yii;
use app\modules\zakaz\models\Zakaz;
use app\modules\driver\models\DriverSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\modules\admin\rbac\Rbac;


/**
 * DriverController implements the CRUD actions for Zakaz model.
 */
class DriverController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
        'access' => [
        'class' => AccessControl::className(),                
        'rules' => [
        [
        'actions' => ['index'],
        'allow' => true,
        'roles' => ['admin'],
        ],
        [
        'actions' => ['view'],
        'allow' => true,
        'roles' => ['admin'],
        ],
        [
        'actions' => ['create'],
        'allow' => true,
        'roles' => ['admin'],
        ],
        [
        'actions' => ['update'],
        'allow' => true,
        'roles' => ['admin'],
        ],
        [
        'actions' => ['delete'],
        'allow' => true,
        'roles' => ['admin'],
        ],
        [
        'actions' => ['myzakaz'],
        'allow' => true,
        'roles' => ['admin'],
        ],
        [
        'actions' => ['touk'],
        'allow' => true,
        'roles' => ['admin'],
        ],

          [
        'actions' => ['marwrut'],
        'allow' => true,
        'roles' => ['admin'],
        ],

        [
        'actions' => ['toua'],
        'allow' => true,
        'roles' => ['admin'],
        ],



        ],
        ],
        ];
    }

    public function send_sms($to, $body){
           
       /* $tel = $to;
        $tel = str_replace("(","",$tel);
        $tel = str_replace(")","",$tel);
        $tel = str_replace("-","",$tel);

        $url ='https://api.mblox.com/xms/v1/uaukz12/batches';
        $json = array("from"=>"UA-UK","to"=>array($tel), "body" => $body);
        $json = json_encode($json);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($json),
            'Authorization: Bearer a8e3911d7b744f50b95e80db0a727cee')
        );
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        $result = curl_exec($ch);
        curl_close($ch);*/

   

    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['toua']);
    }


     public function send_mail($to, $body){
        $mailer = Yii::$app->get('mailer');
        $message = $mailer->compose()
        ->setTo($to)
        ->setFrom("info@ua-uk.com")
        ->setSubject("Ua-Uk info")
        ->setTextBody($body)
        ->send();

     }

    public function actionTouk()
    {     
          

        $searchModel = new DriverSearch();
        $searchModel->to_county = 1;
        $searchModel->status = 0;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $q = (int)$_GET['id'];
        if($q > 0){
            $model = Zakaz::find()->where(['id' => $q])->one();            
            $model->status = 1;
            $model->driver_id = Yii::$app->user->id;
            if($model->save(false)){                
                $this->send_sms($model->from_tel1,'Ваш заказ принят! Ожидайте курьера');
                $this->send_mail($model->from_mail,'Ваш заказ принят! Ожидайте курьера');
            }
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'FLASH_ZAKAZ_PRINYAT'));

        }

        return $this->render('touk', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            ]);
    }


    public function actionToua()
    {

        $searchModel = new DriverSearch();
        $searchModel->status = 0;
        $searchModel->to_county = 2;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $q = (int)$_GET['id'];
        if($q > 0){
            $model = Zakaz::find()->where(['id' => $q])->one();            
            $model->status = 1;
            $model->driver_id = Yii::$app->user->id;
            if($model->save(false)){                
                $this->send_sms($model->from_tel1,'Ваш заказ принят! Ожидайте курьера');
                $this->send_mail($model->from_mail,'Ваш заказ принят! Ожидайте курьера');
            }
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'FLASH_ZAKAZ_PRINYAT'));
        }

        return $this->render('toua', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            ]);
    }




    public function actionIndex()
    {
        $searchModel = new DriverSearch();
        $searchModel->to_county = 1;
        $searchModel->status = 0;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $q = (int)$_GET['id'];
        if($q > 0){
            $model = Zakaz::find()->where(['id' => $q])->one();            
            $model->status = 1;
            $model->driver_id = Yii::$app->user->id;
            $model->save(false);
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'FLASH_ZAKAZ_PRINYAT'));
        }

        return $this->render('touk', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            ]);
    }

    public function actionMarwrut() {


            $model = Zakaz::find()->where(['status' => 0])->all();            
            
        return $this->render('marwrut', [            
            'model' => $model,
            ]);


    }


    public function actionMyzakaz()
    {

        
        if ($post = Yii::$app->request->post()) {  
           
        $model = Zakaz::find()->where(['id' => $post[Zakaz][id]])->one();
            if($model->to_county == 2){

                $model->status = 2;

                if($model->save(false)){
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', 'FLASH_ZAKAZ_DOSTAVLEN_NA_NOVUYU_POSHTU_V_UKRAINE')); 
                    $this->send_sms($model->tel,'Код новой почты - '.$post[Zakaz][kod_np]);
                    $this->send_mail($model->from_mail,'Код новой почты - '.$post[Zakaz][kod_np]);
                }
            }

        }


        $searchModel = new DriverSearch();
        $searchModel->status >= 1;
        $searchModel->driver_id = Yii::$app->user->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);



        $q = (int)$_GET['id'];
        if($q > 0){
            $model = Zakaz::find()->where(['id' => $q])->one();            
            $model->status = 0;
            $model->driver_id = 0;
            $model->save(false);
            Yii::$app->getSession()->setFlash('warning', Yii::t('app', 'FLASH_OTKAZALIS_OT_ZAKAZA'));
        }

        return $this->render('myzakaz', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            ]);
    }


    public function actionView($id)
    {
    	$model = $this->findModel($id);
    	if($model->status == 0){

            return $this->render('view', [
                'model' => $model,
                ]);

        }
    }



    /**
     * Updates an existing Zakaz model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    /*public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }*/



    protected function findModel($id)
    {
        if (($model = Zakaz::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\zakaz\models\Zakaz */

$this->title = Yii::t('app', 'Create Zakaz');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Zakazs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


//print_r($model);


?>

 
<div id="right-panel">
    <div>
    <b>Start:</b>
    <select id="start">
      <option value="51.5852896, -0.0336626">51.5852896, -0.0336626</option>
      
    </select>
    <br>
    <b>Waypoints:</b> <br>
    <i>(Ctrl-Click for multiple selection)</i> <br>
    <select multiple id="waypoints">

    <?php foreach($model as $val){
      if(!empty($val->coords)){
        echo '<option value="'.$val->coords.'">'.$val->id.'</option>';
      }
    }

    ?>
      
     
     
    </select>
    <br>
    <b>End:</b>
    <select id="end">
      <option value="51.5270612, 0.0337963">51.5270612, 0.0337963</option>
      
    </select>
    <br>
      <input type="submit" id="submit">
    </div>
    
    </div>
    <br>
    <div id="map" style="width:100%; height: 500px;"></div>
   
   

   <script>
function initMap() {
  var directionsService = new google.maps.DirectionsService;
  var directionsDisplay = new google.maps.DirectionsRenderer;
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 6,
    center: {lat: 41.85, lng: -87.65}
  });
  directionsDisplay.setMap(map);

  document.getElementById('submit').addEventListener('click', function() {
    calculateAndDisplayRoute(directionsService, directionsDisplay);
  });
}

function calculateAndDisplayRoute(directionsService, directionsDisplay) {
  var waypts = [];
  var checkboxArray = document.getElementById('waypoints');
  for (var i = 0; i < checkboxArray.length; i++) {
    if (checkboxArray.options[i].selected) {
      waypts.push({
        location: checkboxArray[i].value,
        stopover: true
      });
    }
  }

  directionsService.route({
    origin: document.getElementById('start').value,
    destination: document.getElementById('end').value,
    waypoints: waypts,
    optimizeWaypoints: true,
    travelMode: google.maps.TravelMode.DRIVING
  }, function(response, status) {
    if (status === google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
      var route = response.routes[0];
      var summaryPanel = document.getElementById('directions-panel');
      summaryPanel.innerHTML = '';
      // For each route, display summary information.
      for (var i = 0; i < route.legs.length; i++) {
        var routeSegment = i + 1;
        summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment +
            '</b><br>';
        summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
        summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
        summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
      }
    } else {
      window.alert('Directions request failed due to ' + status);
    }
  });
}
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAPB5bfvpcEHbf6M0KIYz57iu1IoBZ5U-Y&signed_in=true&callback=initMap"
        async defer></script>
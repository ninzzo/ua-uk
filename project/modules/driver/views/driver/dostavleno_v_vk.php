<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\zakaz\models\Zakaz */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="zakaz-form">

    <?php $form = ActiveForm::begin(); ?>    

 

    <?= $form->field($model, 'id', [
    'template' => '{input}{hint}{error}',  
    ])->textInput(['style' => 'display:none;' ,'maxlength' => true, 'class'=>"form-control"]); ?>

   

    <div class="form-group">
        <?= Html::submitButton('Доставлено', ['class' => 'btn btn-success','data' => [
                                'confirm' => 'Заказ доставлен?',
                                //'method' => 'post',
                          ]]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\main\models\City;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\zakaz\models\ZakazSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Active orders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zakaz-index">

    <h1><?= Html::encode($this->title) ?></h1>

     <p>
        <?= Yii::t('app', 'DRIVER_VAWI_ZAKAZI');?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           
            //'user_id',
            //'driver_id',
           
            [
            'filter' => false,
            'format' => 'raw',
            'header'=>Yii::t('app', 'Data zakaza'),
            'attribute'=>'created_at',
            //'contentOptions' =>['class' => 'table_class','style'=>'display:block;'],
            'content'=>function($data){
                return date("d.m.y H:i", $data->created_at);                
                 }
            ],

            
             [
             'filter' => false,
            'attribute'=>'to_county',
            //'contentOptions' =>['class' => 'table_class','style'=>'display:block;'],
            'content'=>function($data){
                if($data->to_county == 2){return "С Англии в Украину";}
                if($data->to_county == 1){return "С Украины в Англию";}
                
                }
            ],

             'itog_price',
           
             //'from_country',

             [
             'filter' => false,
             'label' => 'Инфо получателя',
                 'format' => 'raw',
                'attribute'=>'to_county',
                //'contentOptions' =>['class' => 'table_class','style'=>'display:block;'],
                'content'=>function($data){
                    if($data->to_county == 2){
                        $city = City::find()->where(['id'=>$data->to_city])->one();
                        $i = 'Украина, '.$city->name_ru.', <strong>НП:</strong> '.$data->nova_poshta.' <strong>Тел:</strong> '.$data->tel.' <strong>ФИО:</strong> '.$data->to_firstname.' '.$data->to_lastname;
                        return  $i;

                    }
                    if($data->to_county == 1){

                        $i = $data->en_full_adress.' <strong>Дом:</strong> '.$data->from_street.' <strong>Тел:</strong> '.$data->tel;
                        return  $i;}
                    
                    }
            ],

             [
             'filter' => false,
            'label' => 'Инфо отправителя',
                 'format' => 'raw',
                'attribute'=>'to_county',
                //'contentOptions' =>['class' => 'table_class','style'=>'display:block;'],
                'content'=>function($data){
                    if($data->to_county == 2){

                        $i = $data->from_city.' <strong>Дом:</strong> '.$data->from_street.' <strong>Тел:</strong> '.$data->from_tel1.' <strong>ФИО:</strong> '.$data->from_firstname.' '.$data->from_lastname;
                        return  $i;

                    }
                    if($data->to_county == 1){

                        $i = $data->from_city.' <strong>Дом:</strong> '.$data->from_street.' <strong>Тел:</strong> '.$data->from_tel1;
                        return  $i;}
                    
                    }
            ],


             [
             'filter' => false,
            'attribute'=>'status',
            'header'=>'Доставлено',
            'format' => 'raw',
            'contentOptions' =>['style'=>'width: 200px;'],
            //'contentOptions' =>['class' => 'table_class','style'=>'display:block;'],
         
                'content' => function($model, $key, $index, $column) {

                    if($model[to_county] == 2){
                        if($model[status] == 1){
                            return $this->render('dostavleno', ['model' => $model]);
                        }
                        if($model[status] == 2){
                            return 'Kod novoy po4ti';
                        }
                    }

                    if($model[to_county] == 1){
                        if($model[status] == 1){
                            return $this->render('dostavleno_v_vk', ['model' => $model]);
                        }
                        if($model[status] == 2){
                            return '';
                        }
                    }
                },
            ],



            [
             'filter' => false,
            'attribute'=>'status',
            //'header'=>'Отказаться',
            //'contentOptions' =>['class' => 'table_class','style'=>'display:block;'],
            'content'=>function($data){
                if($data->status == 1){
                        return Html::a(Yii::t('app', 'otkaz'), ['myzakaz', 'id' => $data->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => Yii::t('app', 'Отказаться от заказа?'),
                                //'method' => 'post',
                          ]]);
                    }

                if($data->status == 2){
                        return '<span class="label label-info">Доставлен</span>';
                    }
               
                
                }


                
               
                
                
            ],


             /*'from_city',
             'from_street',
             'from_paradniy',
             'from_desc',
             
             'tel',*/
             //'name',
           /*  'to_county',
             'to_city',
             'to_desc',
             'status',
            
             'nova_poshta',
             'to_tel2',
             'parcel_ves',
             'parcel_visota',
             'parcel_wirina',
             'parcel_dlina',
             'from_tel1',
             'from_tel2',
             'from_time1',
             'from_time2',
             'en_full_adress',
             'en_street_number',
             'en_street_name',
             'en_city',
             'en_postal_code',
             'en_country',
             'from_firstname',
             'from_lastname',
             'to_firstname',
             'to_lastname',*/

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\zakaz\models\Zakaz */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="zakaz-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'driver_id')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'from_country')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'from_city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'from_street')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'from_paradniy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'from_desc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'plus_money')->textInput() ?>

    <?= $form->field($model, 'tel')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'to_county')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'to_city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'to_desc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'itog_price')->textInput() ?>

    <?= $form->field($model, 'nova_poshta')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'to_tel2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'parcel_ves')->textInput() ?>

    <?= $form->field($model, 'parcel_visota')->textInput() ?>

    <?= $form->field($model, 'parcel_wirina')->textInput() ?>

    <?= $form->field($model, 'parcel_dlina')->textInput() ?>

    <?= $form->field($model, 'from_tel1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'from_tel2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'from_time1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'from_time2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'en_full_adress')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'en_street_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'en_street_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'en_city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'en_postal_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'en_country')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'from_firstname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'from_lastname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'to_firstname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'to_lastname')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

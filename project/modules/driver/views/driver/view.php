<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\zakaz\models\Zakaz */

$this->title = Yii::t('app', 'Prosmotr_zakaza_#').$model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Zakazs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


use branchonline\lightbox\Lightbox;

?>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="text-center">
                <i class="fa fa-search-plus pull-left icon"></i>
                <h2><?php echo Yii::t('app', 'Zakaz_nomber_#').' '.$model->id.'<br>Штрих код - U'.$model->created_at;?></h2>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-12 col-md-4 col-lg-4 pull-left">
                    <div class="panel panel-success height">
                        <div class="panel-heading"><?php echo Yii::t('app', 'zakaz_kurera_s:');?></div>
                        <div class="panel-body">
                        <?php
if($model->to_county == 2){
    echo '<table>
    <tr><td class="left_td">'.Yii::t('app', 'Country').'<td>'.Yii::t('app', 'England').'
    <tr><td class="left_td">'.Yii::t('app', 'Town').'<td>'.$model->from_city.'
    <tr><td class="left_td">'.Yii::t('app', 'Post code').'<td>'.$model->from_postcode.'
    <tr><td class="left_td">'.Yii::t('app', 'Nomer').'<td>'.$model->from_paradniy.'
    <tr><td class="left_td">'.Yii::t('app', 'Ulica').'<td>'.$model->from_street.'
    <tr><td class="left_td">'.Yii::t('app', 'Name').'<td>'.$model->from_firstname.' '.$model->from_lastname.'
    <tr><td class="left_td">'.Yii::t('app', 'Telefon1').'<td>'.$model->from_tel1.'
   
    <tr><td class="left_td">'.Yii::t('app', 'Mail').'<td>'.$model->from_mail.'
  
    </table>';

 
}

if($model->to_county == 1){
    echo '<table>
    <tr><td class="left_td">'.Yii::t('app', 'Country').'<td>'.Yii::t('app', 'Ukraine').'
    <tr><td class="left_td">'.Yii::t('app', 'Town').'<td>'.$model->from_city.'
    <tr><td class="left_td">'.Yii::t('app', 'Post code').'<td>'.$model->from_postcode.'
     <tr><td class="left_td">'.Yii::t('app', 'Ulica').'<td>'.$model->from_street.'
    <tr><td class="left_td">'.Yii::t('app', 'Nomer').'<td>'.$model->from_paradniy.'   
    <tr><td class="left_td">'.Yii::t('app', 'Name').'<td>'.$model->from_firstname.' '.$model->from_lastname.'
    <tr><td class="left_td">'.Yii::t('app', 'Telefon1').'<td>'.$model->from_tel1.'
    
    <tr><td class="left_td">'.Yii::t('app', 'Mail').'<td>'.$model->from_mail.'

    </table>';

 
}
?>

                           
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4 col-lg-4">
                    <div class="panel panel-info height">
                        <div class="panel-heading"><?php echo Yii::t('app', 'Dostavit posilku v:');?></div>
                        <div class="panel-body">
                            <?php
if($model->to_county == 2){

echo '<table>
    <tr><td class="left_td">'.Yii::t('app', 'Country').'<td>'.Yii::t('app', 'Ukraine').'
    <tr><td class="left_td">'.Yii::t('app', 'Town').'<td>'.$model->to_city.'
    <tr><td class="left_td">'.Yii::t('app', 'Nova poshta').'<td>'.$model->nova_poshta.'   
    <tr><td class="left_td">'.Yii::t('app', 'Name').'<td>'.$model->to_firstname.' '.$model->to_lastname.'
    <tr><td class="left_td">'.Yii::t('app', 'Telefon1').'<td>'.$model->tel.'
    <tr><td class="left_td">'.Yii::t('app', 'Telefon2').'<td>'.$model->to_tel2.'
    </table>';

}

if($model->to_county == 1){
     echo '<table>
    <tr><td class="left_td">'.Yii::t('app', 'England').'<td>'.Yii::t('app', 'England').'
    <tr><td class="left_td">'.Yii::t('app', 'Town').'<td>'.$model->en_city.'
    <tr><td class="left_td">'.Yii::t('app', 'Post code').'<td>'.$model->en_postal_code.'
     <tr><td class="left_td">'.Yii::t('app', 'Ulica').'<td>'.$model->en_street_name.'
    <tr><td class="left_td">'.Yii::t('app', 'Nomer').'<td>'.$model->en_street_number.'
   
    <tr><td class="left_td">'.Yii::t('app', 'Name').'<td>'.$model->from_firstname.' '.$model->from_lastname.'
    <tr><td class="left_td">'.Yii::t('app', 'Telefon1').'<td>'.$model->from_tel1.'
    <tr><td class="left_td">'.Yii::t('app', 'Telefon2').'<td>'.$model->from_tel2.'
    </table>';

}
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4 col-lg-4">
                    <div class="panel panel-danger height">
                        <div class="panel-heading"><?= Yii::t('app', 'Vremya dostavki')?></div>
                        <div class="panel-body">
                        <?php
                            if($model->to_county == 2){
    
    echo '<table>    
    <tr><td class="left_td">'.Yii::t('app', 'Data zakaza').'<td>'.date("d.m.Y H:i",$model->created_at).'
    <tr><td class="left_td"><td>
    
    <tr><td class="left_td">'.Yii::t('app', 'Ojidayut s').'<td>'.$model->from_time1.'
    <tr><td class="left_td">'.Yii::t('app', 'Ojidayut po').'<td>'.$model->from_time2.'   
    </table>';
 
}

if($model->to_county == 1){

    echo '<table>    
    <tr><td class="left_td">'.Yii::t('app', 'Data zakaza').'<td>'.date("d.m.Y H:i",$model->created_at).'
    <tr><td class="left_td"><td>
    <tr><td class="left_td">'.Yii::t('app', 'Posilka budet otpravlena vam na novuyu pushtu').'<td>
    </table>';
}
?>
                        </div>
                    </div>
                </div>
 <div class="clearfix"></div>
                <div class="col-xs-6 col-md-6 col-lg-6">
                    <div class="panel panel-default height">
                        <div class="panel-heading"><?= Yii::t('app', 'O posilke')?></div>
                        <div class="panel-body">

                        <style>
                        td>a>img{width:150px;}
                        </style>
                        <?php
          

    echo '<table>       
    
    <tr><td class="left_td">'.Yii::t('app', 'Ves posilki').'<td>'.$model->parcel_ves.' КГ
    <tr><td class="left_td">'.Yii::t('app', 'Mest(sumok_korobok)').'<td>'.$model->parcel_visota;
    
    if($model->parcel_wirina == 1){
        echo '
    <tr><td class="left_td">'.Yii::t('app', 'Novie_veshi').'<td>'.Yii::t('app', 'Yes').'
    <tr><td class="left_td">'.Yii::t('app', 'Ves_novix').'<td>'.$model->ocenka.' КГ
    <tr><td class="left_td">'.Yii::t('app', 'Ocenka novix').'<td>'.$model->parcel_ves.' £';
    }


    if($model->gabarit == 1){
                echo '
                <tr><td class="left_td">'.Yii::t('app', 'Gabaritnie_veshi').'<td>'.Yii::t('app', 'Yes');
                if(!empty($model->image)){
                    echo '<tr><td class="left_td">'.Yii::t('app', 'image').'<td>';
                echo Lightbox::widget([
                'files' => [
                    [
                        'thumb' => Yii::$app->request->baseUrl.'/project/uploads/'.$model->image,
                        'original' => Yii::$app->request->baseUrl.'/project/uploads/'.$model->image,
                        'title' => 'Photo',
                    ],
                ]
            ]);
                ;}
                
                 if(!empty($model->image2)){
                    echo '<tr><td class="left_td">'.Yii::t('app', 'image2').'<td>';
                echo Lightbox::widget([
                'files' => [
                    [
                        'thumb' => Yii::$app->request->baseUrl.'/project/uploads/'.$model->image2,
                        'original' => Yii::$app->request->baseUrl.'/project/uploads/'.$model->image2,
                        'title' => 'Photo',
                    ],
                ]
            ]);
                ;}
    
       }
    echo '
    <tr><td class="left_td">'.Yii::t('app', 'Opisanie_posilki').'<td>'.$model->parcel_dlina.'
    </table>';
 



?>
                        </div>
                    </div>
                </div>



                <div class="col-xs-6 col-md-6 col-lg-6">
                    <div class="panel panel-default height">
                        <div class="panel-heading"><?= Yii::t('app', 'Maps')?></div>
                        <div class="panel-body">
                            <div id="map1"></div>
                        </div>
                    </div>
                </div>
              
            </div>
        </div>
    </div>
   

    <?php 

if($model->to_county == 2){$to = 'toua';}
if($model->to_county == 1){$to = 'touk';}
   ?>
        
        <?= Html::a(Yii::t('app', 'prinyat'), [$to, 'id' => $model->id], [
            'class' => 'btn btn-primary btn-lg',
            'style' => 'float:right; width:500px;',
            'data' => [
                'confirm' => Yii::t('app', 'prinyat_zakaz?'),
                //'method' => 'post',
            ],
        ]) ?>
</div>

<style>
.left_td {
    font-weight: bold;
    min-width: 90px;
    text-align: left;
}

.height {
    min-height: 200px;
}

.icon {
    font-size: 47px;
    color: #5CB85C;
}

.iconbig {
    font-size: 77px;
    color: #5CB85C;
}

.table > tbody > tr > .emptyrow {
    border-top: none;
}

.table > thead > tr > .emptyrow {
    border-bottom: none;
}

.table > tbody > tr > .highrow {
    border-top: 3px solid;
}

td {text-align: center;}
</style>


<style>
      
      #map1 {
        width: 100%;
        height: 300px;
      }
    </style>

   <script>
<?php 
$coords = explode(",", $model->coords);
$x= $coords[0];
$y= $coords[1];
?>
function initMap() {
  var myLatLng = {lat: <?=$x ?>, lng: <?=$y ?>};

  var map = new google.maps.Map(document.getElementById('map1'), {
    zoom: 12,
    center: myLatLng
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: 'Hello World!'
  });
}

    </script>
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAMzRlocBxBIvZEkONthBf0RPbNT4dx0t8&signed_in=true&callback=initMap"></script>
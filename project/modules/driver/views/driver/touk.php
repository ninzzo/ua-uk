<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\main\models\City;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\zakaz\models\ZakazSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Active_orders_to_uk');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zakaz-index">

    <h1><?= Html::encode($this->title) ?></h1>

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           
            //'user_id',
            //'driver_id',
           
            [
            'filter' => false,
            'format' => 'raw',
            'attribute'=>'created_at',
            //'contentOptions' =>['class' => 'table_class','style'=>'display:block;'],
            'content'=>function($data){
                return date("d.m.y H:i", $data->created_at);                
                 }
            ],

            
             [
             'filter' => false,
            'attribute'=>'to_county',
            //'contentOptions' =>['class' => 'table_class','style'=>'display:block;'],
            'content'=>function($data){
                return Yii::t('app', 'V England');}
            ],

            [            
            'attribute'=>'parcel_visota',
            'label' => Yii::t('app', 'Mest(sumok_korobok)'),
            //'contentOptions' =>['class' => 'table_class','style'=>'display:block;'],
            'content'=>function($data){
                return $data->parcel_visota;}
            ],

            'parcel_ves',

          

             [
             'filter' => false,
              'label' => 'Инфо отправителя',
                 'format' => 'raw',
                'attribute'=>'from_firstname',                
                'content'=>function($data){ return  $data->from_firstname.' '.$data->from_lastname;}                  
            ],

            [             
              'label' => 'Телефон отправителя',            
                'attribute'=>'from_tel1',                
                'content'=>function($data){ return  $data->from_tel1;}                  
            ],



            ['class' => 'yii\grid\ActionColumn',  'template'=>'{view}',],

        ],
    ]); ?>
</div>



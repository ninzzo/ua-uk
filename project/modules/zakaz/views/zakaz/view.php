<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\zakaz\models\Zakaz */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Zakazs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zakaz-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'driver_id',
            'created_at',
            'updated_at',
            'from_country',
            'from_city',
            'from_street',
            'from_paradniy',
            'from_desc',
            'plus_money',
            'tel',
            'name',
            'to_county',
            'to_city',
            'to_desc',
            'status',
            'itog_price',
            'nova_poshta',
            'to_tel2',
            'parcel_ves',
            'parcel_visota',
            'parcel_wirina',
            'parcel_dlina',
            'from_tel1',
            'from_tel2',
            'from_time1',
            'from_time2',
            'en_full_adress',
            'en_street_number',
            'en_street_name',
            'en_city',
            'en_postal_code',
            'en_country',
            'from_firstname',
            'from_lastname',
            'to_firstname',
            'to_lastname',
        ],
    ]) ?>

</div>

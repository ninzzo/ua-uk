<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

//use kartik\date\DatePicker;
use yii\helpers\Url;

use yii\widgets\MaskedInput;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;

/* @var $this yii\web\View */
$this->title = Yii::$app->name;
?>



<div class="main-default-index">

  <div class="row">
    <div class="col-lg-5">






    </div>
    <div class="col-lg-7">

      <?php 
      
      $form = ActiveForm::begin(); 


      ?>
      <?= $form->errorSummary($model); ?>





      <h3><?php echo Yii::t('app', 'zakaz_dostavki_s:');?></h3> 
      <?= $form->field($model, 'to_county')->dropDownList([
        '2' => 'Англии в Украину', 
        '1' => 'Украины в Англию',      
        ], ['id'=>'to_country', 'prompt'=>'Выберите',])->label(false);
        ?>

      </select>


      <div class="panel-group" id="accordion">
        <!-- 1 панель -->
        <div class="panel panel-default">
          <!-- Заголовок 1 панели -->
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" id="collapse1" href="#collapseOne"><?php echo Yii::t('app', 'zakaz_dostavki_to:');?><i class="indicator glyphicon glyphicon-chevron-down  pull-right"></i></a>
              
            </h4>
          </div>
          <div id="collapseOne" class="panel-collapse collapse">
            <!-- Содержимое 1 панели -->
            <div class="panel-body">
              <div style="display:none;" id="to_en">

                <?= $form->field($model, 'en_full_adress')->textInput(['id'=>'autocomplete', 'onFocus'=>"geolocate()"])->label("Postcode or adress") ?>
                <div class="row">
                  <div class="col-lg-3">
                    <?= $form->field($model, 'en_street_number')->textInput(['type' => 'number','min'=>1,'id'=>'street_number','disabled'=>true])->label("House number") ?>
                  </div>
                  <div class="col-lg-9">
                    <?= $form->field($model, 'en_street_name')->textInput(['id'=>'route','disabled'=>true])->label("Street address") ?>
                  </div>
                </div>


                <div class="row">
                 <div class="col-lg-4">
                  <?= $form->field($model, 'en_city')->textInput(['id'=>'locality','disabled'=>true])->label("City") ?>
                </div>
                <div class="col-lg-4">
                  <?= $form->field($model, 'en_postal_code')->textInput(['id'=>'postal_code','disabled'=>true])->label("PostCode") ?>
                </div>
                <div class="col-lg-4">
                  <?= $form->field($model, 'en_country')->textInput(['id'=>'country','disabled'=>true])->label("Country") ?>
                </div>
              </div>



            </div>

            <div id="to_ukr">
             

               <?php                 
                echo $form->field($model, 'to_city')->widget(Select2::classname(), [
                    'data' => [
                '1' => 'Авангард',        
                ],
                    'language' => 'en',
                    'options' => ['id'=>'city', 'data-toggle'=>'tooltip', 'title'=>"Текст всплывающей подсказки", 'data-placement'=>"left"],
                    'pluginOptions' => [
                        'allowClear' => false,
                    ],
                ]);
              ?>

               <?= $form->field($model, 'nova_poshta')->dropDownList([
                    'Отделение № 1: ул. Абрикосовая, 1' => 'Отделение № 1: ул. Абрикосовая, 1',      
                    ], ['id'=>'nova_poshta'])->label(Yii::t('app', 'otdelenie_nova_poshta'));

                    ?>
             

              </div>
              <div class="row">
                <div class="col-lg-4">
                 
                  <?= $form->field($model, 'to_firstname')->textInput() ?>
                  </div>

                  <div class="col-lg-4">
                  <?= $form->field($model, 'to_lastname')->textInput() ?>

                </div>

                </div>

                <div class="row">
                <div class="col-lg-4">
                  <?= $form->field($model, 'tel')->widget(\yii\widgets\MaskedInput::className(), [
    'mask' => '+9(9999)999-9999',
])->textInput()->label("Тел1 получателя") ?>
                </div>

                <div class="col-lg-4">
                  <?= $form->field($model, 'to_tel2')->textInput()->widget(\yii\widgets\MaskedInput::className(), [
    'mask' => '+9(9999)999-9999',
])->label("Тел2 получателя") ?>
                </div>

              </div>


              <?= $form->field($model, 'to_desc')->textarea(['rows' => 2, 'cols' => 5]) ?>

<div class="row">
 <div class="col-lg-12">
         <a style="float:right;" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="btn btn-info btn-xs" aria-expanded="true">Далее</a>
            </div>
            </div>

            </div>
          </div>
        </div>
        <!-- 2 панель -->
        <div class="panel panel-default">
          <!-- Заголовок 2 панели -->
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><?php echo Yii::t('app', 'o_posilke');?><i class="indicator glyphicon glyphicon-chevron-down  pull-right"></i></a>
                          </h4>
          </div>
          <div id="collapseTwo" class="panel-collapse collapse">
            <!-- Содержимое 2 панели -->
            <div class="panel-body">



            <div class="row">
 <div class="col-lg-12">
         <a style="float:right;" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="btn btn-info btn-xs" aria-expanded="true">Назад</a>
            </div>
            </div>


          
             <div class="row">
              <div class="col-lg-3">
                <?php $model->parcel_ves = 1;?>
                <?= $form->field($model, 'parcel_ves')->textInput(['type' => 'number','min'=>1, 'id'=>'ves','data-toggle'=>'tooltip', 'title'=>"Цена посылки зависит от веса. Больше 10 КГ - скидки!", 'data-placement'=>"top"])->label('Вес посылки(КГ)') ?>

              </div>
              <div class="col-lg-3">
                <?= $form->field($model, 'parcel_visota')->textInput() ?>
              </div>

              <div class="col-lg-3">
                <?= $form->field($model, 'parcel_wirina')->textInput() ?>
              </div>

              <div class="col-lg-3">
                <?= $form->field($model, 'parcel_dlina')->textInput() ?>
              </div>

            </div>

            <h2><?php echo Yii::t('app', 'Ocenka gruza');?> - £ <span id="cena">1</span></h2> 


   <div class="row">
 <div class="col-lg-12">
         <a style="float:right;" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="btn btn-info btn-xs" aria-expanded="true">Далее</a>
            </div>
            </div>

           

          </div>
        </div>
      </div>
      <!-- 3 панель -->
      <div class="panel panel-default">
        <!-- Заголовок 3 панели -->
        <div class="panel-heading">
          <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><?php echo Yii::t('app', 'gde_zabrat_parcel');?>
            <i class="indicator glyphicon glyphicon-chevron-down  pull-right"></i>
            </a>

          </h4>
        </div>
        <div id="collapseThree" class="panel-collapse collapse">
          <!-- Содержимое 3 панели -->
          <div class="panel-body">

            <div class="row">
 <div class="col-lg-12">
         <a style="float:right;" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="btn btn-info btn-xs" aria-expanded="true">Назад</a>
            </div>
            </div>

            <div class="row">

              <div class="col-lg-4">
                <?= $form->field($model, 'from_city')->textInput()->label("Город отправителя") ?>
              </div>
              
              <div class="col-lg-4">
                <?= $form->field($model, 'from_street')->textInput()->label("Номер дома отправителя") ?>
              </div>
            </div>
            <div class="row">   
             <div class="col-lg-4">
              <?= $form->field($model, 'from_firstname')->textInput() ?>
              </div>
              <div class="col-lg-4">
              <?= $form->field($model, 'from_lastname')->textInput() ?>
            </div>
             </div>


            <div class="row">   
            <div class="col-lg-4">
              <?= $form->field($model, 'from_tel1')->widget(\yii\widgets\MaskedInput::className(), [
    'mask' => '+9(9999)999-9999',
])->textInput()->label("Тел1 отправителя") ?>
            </div>
            <div class="col-lg-4">
              <?= $form->field($model, 'from_tel2')->widget(\yii\widgets\MaskedInput::className(), [
    'mask' => '+9(9999)999-9999',
])->textInput()->label("Тел2 отправителя") ?>
            </div>
          </div>

          <?= $form->field($model,'from_time1')->widget(DateTimePicker::className(),['options' => ['placeholder' => 'Select about time ...'],'pluginOptions' => ['format' => 'dd/MM/yyyy H:i', 'autoclose'=>true,]]) ?>
      


          <?php 
         /*     
          echo DateTimePicker::widget([              
              'name' => 'from_time1',
              
              'convertFormat' => true,
              'pluginOptions' => [
                   'autoclose'=>true,
                  'format' => 'dd/MM/yyyy H:i',  
                  'todayHighlight' => true                
              ]
          ]);
*/

           ?>

           <br>

           <?= $form->field($model, 'from_desc')->textarea(['rows' => 2, 'cols' => 5]) ?>
           <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-lg' : 'btn btn-primary btn-lg']) ?>
          </div>

        </div>
      </div>
    </div>
  </div>

  <?php ActiveForm::end(); ?>

</div>
</div>
</div>

<script>
  function country(n){

   $.ajax({
    type     :'POST',
    cache    : false,
    url  : "<?php echo Url::toRoute(['getcity']);?>",
    dataType: 'html',
    data: {country_id: n},
    success  : function(response) {     
     $('#city').html(response);
   }
 });
 }

 function city(n){

   $.ajax({
    type     :'POST',
    cache    : false,
    url  : "<?php echo Url::toRoute(['getotdeli']);?>",  
    dataType: 'html',
    data: {city_id: n},
    success  : function(response) {  
     $('#nova_poshta').html(response);
   }
 });
 }


 $( document ).ready(function() {


    $(function () {
  // инициализировать все элементы на страницы, имеющих атрибут data-toggle="tooltip", как компоненты tooltip
  $('[data-toggle="tooltip"]').tooltip()
})

function toggleChevron(e) {
    $(e.target)
        .prev('.panel-heading')
        .find("i.indicator")
        .toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
}
$('#accordion').on('hidden.bs.collapse', toggleChevron);
$('#accordion').on('shown.bs.collapse', toggleChevron);



  $( "#to_country" ).change(function() {    



    if($("select#to_country").val() == 2){

        
        
      $("#to_country [value='2']").prop("selected", true);
      country(2);
       $('#to_en').hide();
      $('#to_ukr').show();
     

      $('#collapse1')[0].click();
      $('#collapse1').attr("id", "asd");

      

    }
    if($("select#to_country").val() == 1){

         

      $("#to_country [value='1']").prop("selected", true);
      country(1);

      $('#to_en').show();
      $('#to_ukr').hide();

      $('#collapse1')[0].click();
      $('#collapse1').attr("id", "asd");

     
      

    }
  });



        




  $( "#city" ).change(function() { 
    if($("select#to_country").val() == 2){
      city($("select#city").val());
    }
  });


  $( "#ves" ).change(function() {
    if($("#ves").val() < 10){$("#cena").html(2.5*$("#ves").val());}
    if($("#ves").val() >= 10 && $("#ves").val() < 20){$("#cena").html(2*$("#ves").val());}
    if($("#ves").val() >= 20){$("#cena").html(1.5*$("#ves").val());}
  });

//start page    
country(2);

});


</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAMzRlocBxBIvZEkONthBf0RPbNT4dx0t8&signed_in=true&libraries=places&callback=initAutocomplete"
async defer></script>


<script>
// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

var placeSearch, autocomplete;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  
  country: 'long_name',
  postal_code: 'short_name'
};

function initAutocomplete() {
  // Create the autocomplete object, restricting the search to geographical
  // location types.
  autocomplete = new google.maps.places.Autocomplete(
    /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
    {types: ['geocode']});

  // When the user selects an address from the dropdown, populate the address
  // fields in the form.
  autocomplete.addListener('place_changed', fillInAddress);
}

// [START region_fillform]
function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  for (var component in componentForm) {
    document.getElementById(component).value = '';
    document.getElementById("street_number").disabled = false;
    //document.getElementById(component).disabled = false;
  }

  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
    $( "#street_number" ).focus();
  }
}
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete.setBounds(circle.getBounds());
    });
  }
}
// [END region_geolocation]

</script>

 
<!--AIzaSyD65IU2SRdBlMcZ1_2t4fiATm2XvLN0-yQ-->

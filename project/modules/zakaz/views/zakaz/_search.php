<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\zakaz\models\ZakazSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="zakaz-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'driver_id') ?>

    <?= $form->field($model, 'created_at') ?>

    <?= $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'from_country') ?>

    <?php // echo $form->field($model, 'from_city') ?>

    <?php // echo $form->field($model, 'from_street') ?>

    <?php // echo $form->field($model, 'from_paradniy') ?>

    <?php // echo $form->field($model, 'from_desc') ?>

    <?php // echo $form->field($model, 'plus_money') ?>

    <?php // echo $form->field($model, 'tel') ?>

    <?php // echo $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'to_county') ?>

    <?php // echo $form->field($model, 'to_city') ?>

    <?php // echo $form->field($model, 'to_desc') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'itog_price') ?>

    <?php // echo $form->field($model, 'nova_poshta') ?>

    <?php // echo $form->field($model, 'to_tel2') ?>

    <?php // echo $form->field($model, 'parcel_ves') ?>

    <?php // echo $form->field($model, 'parcel_visota') ?>

    <?php // echo $form->field($model, 'parcel_wirina') ?>

    <?php // echo $form->field($model, 'parcel_dlina') ?>

    <?php // echo $form->field($model, 'from_tel1') ?>

    <?php // echo $form->field($model, 'from_tel2') ?>

    <?php // echo $form->field($model, 'from_time1') ?>

    <?php // echo $form->field($model, 'from_time2') ?>

    <?php // echo $form->field($model, 'en_full_adress') ?>

    <?php // echo $form->field($model, 'en_street_number') ?>

    <?php // echo $form->field($model, 'en_street_name') ?>

    <?php // echo $form->field($model, 'en_city') ?>

    <?php // echo $form->field($model, 'en_postal_code') ?>

    <?php // echo $form->field($model, 'en_country') ?>

    <?php // echo $form->field($model, 'from_firstname') ?>

    <?php // echo $form->field($model, 'from_lastname') ?>

    <?php // echo $form->field($model, 'to_firstname') ?>

    <?php // echo $form->field($model, 'to_lastname') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

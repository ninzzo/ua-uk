<?php

namespace app\modules\zakaz\models;

use Yii;

/**
 * This is the model class for table "zakaz".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $driver_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $from_country
 * @property string $from_city
 * @property string $from_street
 * @property string $from_paradniy
 * @property string $from_desc
 * @property integer $plus_money
 * @property integer $tel
 * @property integer $name
 * @property string $to_county
 * @property string $to_city
 * @property string $to_desc
 * @property integer $status
 * @property integer $itog_price
 * @property string $nova_poshta
 * @property string $to_tel2
 * @property integer $parcel_ves
 * @property integer $parcel_visota
 * @property integer $parcel_wirina
 * @property integer $parcel_dlina
 * @property string $from_tel1
 * @property string $from_tel2
 * @property string $from_time1
 * @property string $from_time2
 * @property string $en_full_adress
 * @property string $en_street_number
 * @property string $en_street_name
 * @property string $en_city
 * @property string $en_postal_code
 * @property string $en_country
 * @property string $from_firstname
 * @property string $from_lastname
 * @property string $to_firstname
 * @property string $to_lastname
 */
class Zakaz extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zakaz';
    }

    /*public function afterSave($insert, $changedAttributes){
    parent::afterSave($insert, $changedAttributes);
 
    if ( $this->scenario == 'uk_ua' ) {

                $this->from_tel1 = '44'.$this->from_tel1;
                $this->tel = '38'.$this->from_tel1;

                echo $this->tel;
                
                
                
            }
    }*/


    /*public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            
            if ( $this->scenario == 'uk_ua' ) {

                //$this->from_tel1 = '44'.$this->from_tel1;
                //$this->tel = '38'.$this->from_tel1;

                //echo $this->tel;
                
                
                //die();
            }

            return true;
        } else {
            return false;
        }
    }*/
    




    public function rules()
    {
        return [
            [['from_mail', 'from_country', 'from_paradniy','from_city', 'from_street', 'tel', 'parcel_ves', 'from_tel1', 'from_time1', 'from_firstname', 'from_lastname', 'to_firstname', 'to_lastname', 'to_firstname', 'from_postcode'], 'required'],

            [['en_postal_code', 'en_street_number', 'en_full_adress'], 'required', 'on'=>'ua_uk'],
            [['nova_poshta'], 'required', 'on'=>'uk_ua'],


             [['kod_np'], 'required', 'on'=>'dostavleno'],
            
            ['from_mail', 'email'],
            [['tel', 'from_tel1'], 'string', 'min' => 12],

            [['image', 'image2','coords'], 'safe'],
            [['image', 'image2'], 'file'],


            [['user_id', 'driver_id', 'created_at', 'updated_at', 'plus_money', 'status', 'itog_price', 'parcel_ves', 'parcel_visota', 'parcel_wirina'], 'integer'],
            [['kod_np','cena','new_ves','gabarit','otsledit','from_country', 'from_city', 'from_street', 'from_paradniy', 'to_city', 'nova_poshta', 'en_full_adress', 'en_street_name'], 'string', 'max' => 255],
            [['from_desc', 'to_desc', 'parcel_dlina'], 'string', 'max' => 700],
            [['from_tel1', 'from_tel2', 'from_time1', 'from_time2', 'en_city', 'en_postal_code', 'from_postcode','en_country'], 'string', 'max' => 75],
            [['en_street_number'], 'string', 'max' => 20],
            [['from_firstname', 'from_lastname', 'to_firstname', 'to_lastname'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),

            'kod_np' => Yii::t('app', 'kod_np'),
            'cena' => Yii::t('app', 'cena'),
            'new_ves' => Yii::t('app', 'new_ves'),
            'gabarit' => Yii::t('app', 'gabarit'),
            'otsledit' => Yii::t('app', 'otsledit'),
            'image' => Yii::t('app', 'image'),
            'image' => Yii::t('app', 'image2'),

            'user_id' => Yii::t('app', 'User ID'),
            'driver_id' => Yii::t('app', 'Driver ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'from_country' => Yii::t('app', 'From Country'),
            'from_city' => Yii::t('app', 'From City'),
            'from_street' => Yii::t('app', 'From Street'),
            'from_paradniy' => Yii::t('app', 'From Paradniy'),
            'from_desc' => Yii::t('app', 'From Desc'),
            'plus_money' => Yii::t('app', 'Plus Money'),
            'tel' => Yii::t('app', 'Tel'),
            'name' => Yii::t('app', 'Name'),
            'from_mail' => Yii::t('app', 'from_mail'),
            'to_county' => Yii::t('app', 'To County'),
            'to_city' => Yii::t('app', 'To City'),
            'to_desc' => Yii::t('app', 'To Desc'),
            'status' => Yii::t('app', 'Status'),
            'itog_price' => Yii::t('app', 'Itog Price'),
            'nova_poshta' => Yii::t('app', 'Nova Poshta'),
            'to_tel2' => Yii::t('app', 'To Tel2'),
            'parcel_ves' => Yii::t('app', 'Parcel Ves'),
            'parcel_visota' => Yii::t('app', 'Parcel Visota'),
            'parcel_wirina' => Yii::t('app', 'Parcel Wirina'),
            'parcel_dlina' => Yii::t('app', 'Parcel Dlina'),
            'from_tel1' => Yii::t('app', 'From Tel1'),
            'from_tel2' => Yii::t('app', 'From Tel2'),
            'from_time1' => Yii::t('app', 'From Time1'),
            'from_time2' => Yii::t('app', 'From Time2'),
            'en_full_adress' => Yii::t('app', 'En Full Adress'),
            'en_street_number' => Yii::t('app', 'En Street Number'),
            'en_street_name' => Yii::t('app', 'En Street Name'),
            'en_city' => Yii::t('app', 'En City'),
            'en_postal_code' => Yii::t('app', 'En Postal Code'),
            'en_country' => Yii::t('app', 'En Country'),
            'from_firstname' => Yii::t('app', 'From Firstname'),
            'from_lastname' => Yii::t('app', 'From Lastname'),
            'to_firstname' => Yii::t('app', 'To Firstname'),
            'to_lastname' => Yii::t('app', 'To Lastname'),

            'from_postcode' => Yii::t('app', 'from_postcode'),

            
        ];
    }
}

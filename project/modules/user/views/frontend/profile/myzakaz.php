<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\main\models\City;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\zakaz\models\ZakazSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'NAV_ZAKAZ_USER');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zakaz-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>Статусы:<br>
    <span class="label label-default">Новый заказ</span> - заказ только создан и ожидает подтверждения водителя.<br>
    <span class="label label-success">Подтвержден</span> - заказ подтвержден. Ожидайте курьера по указаному вами адресу.<br>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           
            //'user_id',
            //'driver_id',
           
            [
            'filter' => false,
            'format' => 'raw',
            'attribute'=>'created_at',
            //'contentOptions' =>['class' => 'table_class','style'=>'display:block;'],
            'content'=>function($data){
                return date("d.m.y H:i", $data->created_at);                
                 }
            ],

            
             [
             'filter' => false,
            'attribute'=>'to_county',
            //'contentOptions' =>['class' => 'table_class','style'=>'display:block;'],
            'content'=>function($data){
                if($data->to_county == 2){return "С Англии в Украину";}
                if($data->to_county == 1){return "С Украины в Англию";}
                
                }
            ],

             'itog_price',
           
             //'from_country',

             [
             'filter' => false,
             'label' => 'Инфо получателя',
                 'format' => 'raw',
                'attribute'=>'to_county',
                //'contentOptions' =>['class' => 'table_class','style'=>'display:block;'],
                'content'=>function($data){
                    if($data->to_county == 2){
                        $city = City::find()->where(['id'=>$data->to_city])->one();
                        $i = 'Украина, '.$city->name_ru.', <strong>НП:</strong> '.$data->nova_poshta.' <strong>Тел:</strong> '.$data->tel;
                        return  $i;

                    }
                    if($data->to_county == 1){

                        $i = $data->en_full_adress.' <strong>Дом:</strong> '.$data->from_street.' <strong>Тел:</strong> '.$data->tel;
                        return  $i;}
                    
                    }
            ],

          

           


            [
             'filter' => false,
            'attribute'=>'status',
            //'contentOptions' =>['class' => 'table_class','style'=>'display:block;'],
            'content'=>function($data){
                if($data->status == 0){return '<span class="label label-default">Новый заказ</span>';}
                if($data->status == 1){return '<span class="label label-success">Подтвержден</span>';}
                        
                    
               
                
                }
            ],


             /*'from_city',
             'from_street',
             'from_paradniy',
             'from_desc',
             
             'tel',*/
             //'name',
           /*  'to_county',
             'to_city',
             'to_desc',
             'status',
            
             'nova_poshta',
             'to_tel2',
             'parcel_ves',
             'parcel_visota',
             'parcel_wirina',
             'parcel_dlina',
             'from_tel1',
             'from_tel2',
             'from_time1',
             'from_time2',
             'en_full_adress',
             'en_street_number',
             'en_street_name',
             'en_city',
             'en_postal_code',
             'en_country',
             'from_firstname',
             'from_lastname',
             'to_firstname',
             'to_lastname',*/

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

<?php

namespace app\modules\user\models\frontend\form;

use app\modules\user\models\User;
use app\modules\user\Module;
use yii\base\Model;
use yii\db\ActiveQuery;
use Yii;

class ProfileUpdateForm extends Model
{
    public $email;
    public $firstname;
    public $lastname;
    public $tel;

    /**
     * @var User
     */
    private $_user;

    /**
     * @param User $user
     * @param array $config
     */
    public function __construct(User $user, $config = [])
    {
        $this->_user = $user;
        parent::__construct($config);
    }

    public function init()
    {
        $this->email = $this->_user->email;
        $this->firstname = $this->_user->firstname;
        $this->lastname = $this->_user->lastname;
        $this->tel = $this->_user->tel;
        parent::init();
    }



    public function attributeLabels()
    {
        return [
            

            'firstname' => Module::t('module', 'firstname'),
            'lastname' => Module::t('module', 'lastname'),
            'tel' => Module::t('module', 'tel'),
            'nomer_auto' => Module::t('module', 'nomer_auto'),
        ];
    }


    public function rules()
    {
        return [
           /* ['email', 'required'],
            ['email', 'email'],
            [
                'email',
                'unique',
                'targetClass' => User::className(),
                'message' => Module::t('module', 'ERROR_EMAIL_EXISTS'),
                'filter' => function (ActiveQuery $query) {
                        $query->andWhere(['<>', 'id', $this->_user->id]);
                    },
            ],
            ['email', 'string', 'max' => 255],*/

            ['firstname', 'required'],
            ['firstname', 'string', 'min' => 2],
            ['lastname', 'required'],
            ['lastname', 'string', 'min' => 2],
            ['tel', 'required'],
            ['tel', 'string', 'min' => 12],
        ];
    }

    /**
     * @return bool
     */
    public function update()
    {
        if ($this->validate()) {
            $user = $this->_user;
            $user->firstname = $this->firstname;
            $user->lastname = $this->lastname;
            $user->tel = $this->tel;
            return $user->save();
        } else {
            return false;
        }
    }
} 
<?php

namespace app\modules\user\controllers\frontend;

use app\modules\user\models\frontend\form\PasswordChangeForm;
use app\modules\user\models\frontend\form\ProfileUpdateForm;
use app\modules\user\models\User;
use app\modules\user\Module;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

use app\modules\zakaz\models\Zakaz;
use app\modules\driver\models\DriverSearch;

class ProfileController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
        'actions' => ['myzakaz'],
        'allow' => true,
        'roles' => ['driver'],
        ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index', [
            'model' => $this->findModel(),
        ]);
    }


     public function actionMyzakaz()
    {
        
        $searchModel = new DriverSearch();
        //$searchModel->status = 0;
        $searchModel->user_id = Yii::$app->user->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('myzakaz', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            ]);
    }

    public function actionUpdate()
    {
        $user = $this->findModel();
        $model = new ProfileUpdateForm($user);

        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionPasswordChange()
    {
        $user = $this->findModel();
        $model = new PasswordChangeForm($user);

        if ($model->load(Yii::$app->request->post()) && $model->changePassword()) {
            Yii::$app->getSession()->setFlash('success', Module::t('module', 'FLASH_PASSWORD_CHANGE_SUCCESS'));
            return $this->redirect(['index']);
        } else {
            return $this->render('passwordChange', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @return User the loaded model
     */
    private function findModel()
    {
        return User::findOne(Yii::$app->user->identity->getId());
    }
}

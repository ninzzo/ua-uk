<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\date\DatePicker;
use yii\helpers\Url;

use yii\widgets\MaskedInput;
//use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use kartik\file\FileInput;

/* @var $this yii\web\View */
$this->title = Yii::$app->name; ?>


<?php 
// $form = ActiveForm::begin(); 


?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

<style>

div.required label:after {
    content: " *";
    color: red;
} 


  .wizard {
    margin: 20px auto;
    background: #fff;
  }

  .wizard .nav-tabs {
    position: relative;
    margin: 40px auto;
    margin-bottom: 0;
    border-bottom-color: #e0e0e0;
  }

  .wizard > div.wizard-inner {
    position: relative;
  }

  .connecting-line {
    height: 2px;
    background: #e0e0e0;
    position: absolute;
    width: 80%;
    margin: 0 auto;
    left: 0;
    right: 0;
    top: 50%;
    z-index: 1;
  }

  .wizard .nav-tabs > li.active > a, .wizard .nav-tabs > li.active > a:hover, .wizard .nav-tabs > li.active > a:focus {
    color: #555555;
    cursor: default;
    border: 0;
    border-bottom-color: transparent;
  }

  span.round-tab {
    width: 70px;
    height: 70px;
    line-height: 70px;
    display: inline-block;
    border-radius: 100px;
    background: #fff;
    border: 2px solid #e0e0e0;
    z-index: 2;
    position: absolute;
    left: 0;
    text-align: center;
    font-size: 25px;
  }
  span.round-tab i{
    color:#555555;
  }
  .wizard li.active span.round-tab {
    background: #fff;
    border: 2px solid #5bc0de;
    
  }
  .wizard li.active span.round-tab i{
    color: #5bc0de;
  }

  span.round-tab:hover {
    color: #333;
    border: 2px solid #333;
  }

  .wizard .nav-tabs > li {
    width: 25%;
  }

  .wizard li:after {
    content: " ";
    position: absolute;
    left: 46%;
    opacity: 0;
    margin: 0 auto;
    bottom: 0px;
    border: 5px solid transparent;
    border-bottom-color: #5bc0de;
    transition: 0.1s ease-in-out;
  }

  .wizard li.active:after {
    content: " ";
    position: absolute;
    left: 46%;
    opacity: 1;
    margin: 0 auto;
    bottom: 0px;
    border: 10px solid transparent;
    border-bottom-color: #5bc0de;
  }

  .wizard .nav-tabs > li a {
    width: 70px;
    height: 70px;
    margin: 20px auto;
    border-radius: 100%;
    padding: 0;
  }

  .wizard .nav-tabs > li a:hover {
    background: transparent;
  }

  .wizard .tab-pane {
    position: relative;
    padding-top: 50px;
  }

  .wizard h3 {
    margin-top: 0;
  }
  .step1 .row {
    margin-bottom:10px;
  }
  .step_21 {
    border :1px solid #eee;
    border-radius:5px;
    padding:10px;
  }
  .step33 {
    border:1px solid #ccc;
    border-radius:5px;
    padding-left:10px;
    margin-bottom:10px;
  }
  .dropselectsec {
    width: 68%;
    padding: 6px 5px;
    border: 1px solid #ccc;
    border-radius: 3px;
    color: #333;
    margin-left: 10px;
    outline: none;
    font-weight: normal;
  }
  .dropselectsec1 {
    width: 74%;
    padding: 6px 5px;
    border: 1px solid #ccc;
    border-radius: 3px;
    color: #333;
    margin-left: 10px;
    outline: none;
    font-weight: normal;
  }
  .mar_ned {
    margin-bottom:10px;
  }
  .wdth {
    width:25%;
  }
  .birthdrop {
    padding: 6px 5px;
    border: 1px solid #ccc;
    border-radius: 3px;
    color: #333;
    margin-left: 10px;
    width: 16%;
    outline: 0;
    font-weight: normal;
  }


  /* according menu */
  #accordion-container {
    font-size:13px
  }
  .accordion-header {
    font-size:13px;
    background:#ebebeb;
    margin:5px 0 0;
    padding:7px 20px;
    cursor:pointer;
    color:#fff;
    font-weight:400;
    -moz-border-radius:5px;
    -webkit-border-radius:5px;
    border-radius:5px
  }
  .unselect_img{
    width:18px;
    -webkit-user-select: none;  
    -moz-user-select: none;     
    -ms-user-select: none;      
    user-select: none; 
  }
  .active-header {
    -moz-border-radius:5px 5px 0 0;
    -webkit-border-radius:5px 5px 0 0;
    border-radius:5px 5px 0 0;
    background:#F53B27;
  }
  .active-header:after {
    content:"\f068";
    font-family:'FontAwesome';
    float:right;
    margin:5px;
    font-weight:400
  }
  .inactive-header {
    background:#333;
  }
  .inactive-header:after {
    content:"\f067";
    font-family:'FontAwesome';
    float:right;
    margin:4px 5px;
    font-weight:400
  }
  .accordion-content {
    display:none;
    padding:20px;
    background:#fff;
    border:1px solid #ccc;
    border-top:0;
    -moz-border-radius:0 0 5px 5px;
    -webkit-border-radius:0 0 5px 5px;
    border-radius:0 0 5px 5px
  }
  .accordion-content a{
    text-decoration:none;
    color:#333;
  }
  .accordion-content td{
    border-bottom:1px solid #dcdcdc;
  }



  @media( max-width : 585px ) {

    .wizard {
      width: 90%;
      height: auto !important;
    }

    span.round-tab {
      font-size: 16px;
      width: 50px;
      height: 50px;
      line-height: 50px;
    }

    .wizard .nav-tabs > li a {
      width: 50px;
      height: 50px;
      line-height: 50px;
    }

    .wizard li.active:after {
      content: " ";
      position: absolute;
      left: 35%;
    }
  }
</style>


<div class="container">
<center><h2><?php echo Yii::t('app', 'oformlenie_zakaza_iz_uk_to_ua');?>:</h2></center>
  <div class="row">
    <section>
      <div class="wizard">
        <div class="wizard-inner">
          <div class="connecting-line"></div>
          <ul class="nav nav-tabs" role="tablist">

            <li role="presentation" class="active">
              <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="<?php echo Yii::t('app', 'zakaz_dostavki_to:');?>">
                <span class="round-tab">
                  <i class="glyphicon glyphicon-list-alt"></i>
                </span>
              </a>
            </li>

            <li role="presentation" class="disabled">
              <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="<?php echo Yii::t('app', 'o_posilke');?>">
                <span class="round-tab">
                  <i class="glyphicon glyphicon-envelope"></i>
                </span>
              </a>
            </li>
            <li role="presentation" class="disabled">
              <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="<?= Yii::t('app', 'gde_zabrat_parcel')?>">
                <span class="round-tab">
                  <i class="glyphicon glyphicon-user"></i>
                </span>
              </a>
            </li>

            <li role="presentation" class="disabled">
              <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="<?= Yii::t('app', 'complete')?>">
                <span class="round-tab">
                  <i class="glyphicon glyphicon-ok"></i>
                </span>
              </a>
            </li>


          </ul>
        </div>

        <form role="form">
          <div class="tab-content">
            <div class="tab-pane active" role="tabpanel" id="step1">
              <div class="step1">


<h3 style="text-align: center;"><?php echo Yii::t('app', 'zakaz_dostavki_to:');?></h3>


             
 <div class="col-sm-offset-4 col-sm-4">
               <?php                 
                echo $form->field($model, 'to_city')->widget(Select2::classname(), [
            'data' => [
                '1' => 'Авангард',        
                ],
            'language' => 'en',
            'options' => ['id'=>'city', 'data-toggle'=>'tooltip', 'title'=>"Текст всплывающей подсказки", 'data-placement'=>"left"],
            'pluginOptions' => [
                'allowClear' => false,
            ],
        ]);
              ?>

 </div>


 <div class="col-sm-offset-4 col-sm-4">
               <?= $form->field($model, 'nova_poshta')->dropDownList([
              'Отделение № 1: ул. Абрикосовая, 1' => 'Отделение № 1: ул. Абрикосовая, 1',      
              ], ['id'=>'nova_poshta'])->label(Yii::t('app', 'otdelenie_nova_poshta'));

              ?>
       </div>       

           
             <div class="row">
                <div class="col-sm-offset-4 col-sm-2">
                 
                  <?= $form->field($model, 'to_firstname')->textInput()->label(Yii::t('app', 'to_firstname')) ?>
                  </div>

                  <div class="col-sm-2">
                  <?= $form->field($model, 'to_lastname')->textInput()->label(Yii::t('app', 'to_lastname')) ?>

                </div>

          </div>
                 

                 
<div class="row">
             
                <div class="col-sm-offset-4 col-sm-2">
                 <?= $form->field($model, 'tel')->widget(\yii\widgets\MaskedInput::className(), [
    'mask' => '+389999999999',
    'clientOptions' => [        
        //'removeMaskOnSubmit' => true,
        ]
])->textInput(['data-toggle'=>'tooltip', 'title'=>Yii::t('app', 'mejdunarodniy_format'), 'data-placement'=>"top"])->label(Yii::t('app', 'tel_poluch_1')) ?>


                </div>

                <div class="col-sm-2">
                  <?= $form->field($model, 'to_tel2')->textInput()->widget(\yii\widgets\MaskedInput::className(), [
    'mask' => '+389999999999',
    'clientOptions' => [        
        //'removeMaskOnSubmit' => true,
        ]
])->label(Yii::t('app', 'tel_poluch_2')) ?>
                </div>

             </div>
<div class="col-sm-offset-4 col-sm-4">

              <?= $form->field($model, 'to_desc')->textarea(['rows' => 2, 'cols' => 5]) ?>
 </div>
              
                    </div>

                    <div class="col-lg-12">
                    <ul class="list-inline" style="text-align: center;">
                      
                      <li><button type="button" class="btn btn-primary next-step"><?=Yii::t('app', 'next')?></button></li>
                    </ul>
                    </div>


                  </div>
                  <div class="tab-pane" role="tabpanel" id="step2">
                    <div class="step2">

<h3 style="text-align: center;"><?php echo Yii::t('app', 'o_posilke');?></h3>

                     
                    
              <div class="col-sm-offset-4 col-sm-4">
                <?php $model->parcel_ves = 1;?>
                <?= $form->field($model, 'parcel_ves')->textInput(['type' => 'number','min'=>1, 'id'=>'ves','data-toggle'=>'tooltip', 'title'=>Yii::t('app', 'cena_posilki_podskazka'), 'data-placement'=>"top"])->label(Yii::t('app', 'ves_posilki')) ?>

              </div>
             <div class="col-sm-offset-4 col-sm-4">
                <?php $model->parcel_visota = 1;?>
                <?= $form->field($model, 'parcel_visota')->textInput(['type' => 'number','min'=>1, 'data-toggle'=>'tooltip', 'title'=>Yii::t('app', 'kol_sumok_paketov'), 'data-placement'=>"top"])->label(Yii::t('app', 'nada_mest')) ?>
              </div>

              <div class="col-sm-offset-4 col-sm-4">
                <?= $form->field($model, 'parcel_wirina')
                    ->checkbox([
                        'label' => Yii::t('app', 'Novie_veshi'),
                        'id'=>'novie_vewi',
                        'data-toggle'=>'tooltip', 'title'=>Yii::t('app', 'Novie_veshi_podskazka'), 'data-placement'=>"top",
                        'labelOptions' => [
                            'style' => 'padding-top:30px;'
                        ],
                     
                    ]); ?>
              </div>


<div id="new_stuff" style="display:none;">

               <div class="col-sm-offset-4 col-sm-4">
                <?php $model->parcel_ves = 1;?>
                <?= $form->field($model, 'new_ves')->textInput(['type' => 'number','min'=>1, 'id'=>'new_ves'])->label(Yii::t('app', 'new_ves')) ?>
              </div>


              <div class="col-sm-offset-4 col-sm-4">
                
                <?= $form->field($model, 'cena')->textInput(['type' => 'number','min'=>1])->label(Yii::t('app', 'cena')) ?>

              </div>

              </div>




              <div class="col-sm-offset-4 col-sm-4">
                <?= $form->field($model, 'gabarit')
                    ->checkbox([
                        'label' => Yii::t('app', 'gabarit'),
                        'id'=>'gabarit',
                        'data-toggle'=>'tooltip', 'title'=>Yii::t('app', 'gabarit_podskazka'), 'data-placement'=>"top",
                        'labelOptions' => [
                            'style' => 'padding-top:30px;'
                        ],
                     
                    ]); ?>
              </div>


              <div id="gabarit_block" style="display:none;">

             
              <div class="col-sm-offset-4 col-sm-4">
                
                 <?php

                echo $form->field($model, 'image')->widget(FileInput::classname(), 
                	['pluginOptions' => [
				        'showCaption' => true,
				        'showRemove' => false,
				        'showUpload' => false,
				        'browseClass' => 'btn btn-primary btn-block',
				        'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
				        'browseLabel' =>  'Select Photo',
				    ]],
                	['options' => ['accept' => 'image/*', 'multiple' => false],
                ])->label(Yii::t('app', 'image'));

                 ?>

                 <?php

                echo $form->field($model, 'image2')->widget(FileInput::classname(), 
                  ['pluginOptions' => [
                'showCaption' => true,
                'showRemove' => false,
                'showUpload' => false,
                'browseClass' => 'btn btn-primary btn-block',
                'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                'browseLabel' =>  'Select Photo',
            ]],
                  ['options' => ['accept' => 'image/*', 'multiple' => false],
                ])->label(Yii::t('app', 'image2'));

                 ?>

              </div>
              </div>






<script>
$('#novie_vewi').change(function () {


  if($('#novie_vewi').is(':checked')){
    $('#new_stuff').show(); 
  }else{
    $('#new_stuff').hide();
  }

    
 });


$('#gabarit').change(function () {

  if($('#gabarit').is(':checked')){
    $('#gabarit_block').show(); 
  }else{
    $('#gabarit_block').hide();
  }

    
 });
  
</script>
             


 <div class="col-sm-offset-4 col-sm-4">
              <?php
// Usage with ActiveForm and model
/*echo $form->field($model, 'to_city')->widget(FileInput::classname(), [
    'options' => ['accept' => 'image/*', 'multiple' => true],
])->label(Yii::t('app', 'Image'))*/

 ?>

<?php
/*
echo '<br><label>FileInput</label>';
echo FileInput::widget([
    'name' => 'to_city[]',
    'language' => 'ru',
    'options' => ['multiple' => true],
    'pluginOptions' => ['previewFileType' => 'any', 'uploadUrl' => Url::to(['/images']),]
*/
    ?>


    



 </div>
           
<div class="col-sm-offset-4 col-sm-4">
              <?= $form->field($model, 'parcel_dlina')->textarea(['rows' => 2, 'cols' => 5, 'placeholder' => Yii::t('app', 'opisanie posilki_place')])->label(Yii::t('app', 'opisanie posilki')) ?>

              </div>

                    </div>


                     <div class="col-lg-12">
                    <ul class="list-inline"  style="text-align: center;">
                      <li><button type="button" class="btn btn-default prev-step"><?=Yii::t('app', 'nazad')?></button></li>
                      <li><button type="button" class="btn btn-primary next-step"><?=Yii::t('app', 'next')?></button></li>
                    </ul>
                    </div>




                  </div>


                  <div class="tab-pane" role="tabpanel" id="step3">

<h3 style="text-align: center;"><?= Yii::t('app', 'gde_zabrat_parcel')?></h3>



  <div class="row">
                <div class="col-sm-offset-4 col-sm-2">
              <?= $form->field($model, 'from_firstname')->textInput()->label(Yii::t('app', 'from_firstname')) ?>
              </div>
              <div class="col-sm-2">
              <?= $form->field($model, 'from_lastname')->textInput()->label(Yii::t('app', 'from_lastname')) ?>
            </div>

            </div>


                <div class="col-sm-offset-4 col-sm-4">
                 <?= $form->field($model, 'from_postcode')->textInput(['id'=>'autocomplete1', 'onFocus'=>"geolocate1()"])->label(Yii::t('app', 'Post code')) ?>
                   </div>
           
                   <div class="row">
                <div class="col-sm-offset-4 col-sm-2">
                <?= $form->field($model, 'from_country')->textInput(['id'=>'country1'])->label(Yii::t('app', 'Country')) ?>
                   
                  </div>
                  <div class="col-sm-2">
                  <?= $form->field($model, 'from_city')->textInput(['id'=>'locality1'])->label(Yii::t('app', 'Town')) ?>
                 
                  </div>
               
</div>


<div class="row">
<div class="col-sm-offset-4 col-sm-3">
   <?= $form->field($model, 'from_street')->textInput(['id'=>'route1'])->label(Yii::t('app', 'Street address')) ?>
   </div>
   <div class="col-sm-1">
                 <?= $form->field($model, 'from_paradniy')->textInput(['id'=>'street_number1'])->label(Yii::t('app', 'House number')) ?>
                 
                </div>
                 </div>
               
     
              


<div class="row">
                <div class="col-sm-offset-4 col-sm-2">

         
              <?= $form->field($model, 'from_mail')->textInput()->label(Yii::t('app', 'from_mail')) ?>
            </div>
         


          
            <div class="col-lg-2">
              <?= $form->field($model, 'from_tel1')->widget(\yii\widgets\MaskedInput::className(), [
    'mask' => '+449999999999',
    'clientOptions' => [        
        //'removeMaskOnSubmit' => true,
        ]
])->textInput(['data-toggle'=>'tooltip', 'title'=>Yii::t('app', 'mejdunarodniy_format'), 'data-placement'=>"top"])->label(Yii::t('app', 'from_tel1')) ?>
            </div>
          
         
</div>



 <div class="row">
                <div class="col-sm-offset-4 col-sm-2">

          <?= $form->field($model,'from_time1')->widget(DatePicker::classname(),[
      'options' => ['placeholder' => ''],
      'value' => date('d-M-Y'),
      'type' => DatePicker::TYPE_INPUT,   
      
      'pluginOptions' => [

        'autoclose'=>true,
        'format' => 'mm/dd/yyyy',
        'todayHighlight' => TRUE,
    ]
    ]); ?>


  </div>
            <div class="col-lg-2">
          


              <?= $form->field($model, 'from_time2')->dropDownList([
                   
              '08:00 - 15:00' => '08:00 - 15:00',   
              '15:00 - 24:00' => '15:00 - 24:00',   
             
              ], ['id'=>'from_time2'])->label(Yii::t('app', 'from_time2'));

              ?>




    </div>

    </div>
         

<div class="col-sm-offset-4 col-sm-4">
           <?= $form->field($model, 'from_desc')->textarea(['rows' => 2, 'cols' => 5]) ?>
</div>

<div class="col-sm-12">
                   <ul class="list-inline"  style="text-align: center;">
                    <!--<li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                    <li><button type="button" class="btn btn-default next-step">Skip</button></li>
                    <li><button type="button" class="btn btn-primary btn-info-full next-step">Continue</button></li>-->

                    <li><button type="button" class="btn btn-default prev-step"><?=Yii::t('app', 'nazad')?></button></li>
                      <li><button type="button" class="btn btn-primary next-step"><?=Yii::t('app', 'next')?></button></li>
                  </ul>
 </div>


                </div>
                <div class="tab-pane" role="tabpanel" id="complete">
                  <div class="step44">

<div class="col-sm-offset-4 col-sm-4">
                   <?= $form->errorSummary($model); ?>

                   <div class="form-group">
                     <center>
                      <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-lg' : 'btn btn-primary btn-lg']) ?><center>
                    </div>

                    <div class="col-sm-12">
                   <ul class="list-inline"  style="text-align: center;">
                 

                    <li><button type="button" class="btn btn-default prev-step"><?=Yii::t('app', 'nazad')?></button></li>
                     
                  </ul>
                  </div>
 </div>

                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
            </form>
          </div>
        </section>
      </div>
    </div>


    <?php ActiveForm::end(); ?>


    


    <script type="text/javascript">
$(document).ready(function() {
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
});


 function country(n){

   $.ajax({
    type     :'POST',
    cache    : false,
    url  : "<?php echo Url::toRoute(['getcity']);?>",
    dataType: 'html',
    data: {country_id: n},
    success  : function(response) {     
     $('#city').html(response);
   }
 });
 }

 function city(n){

   $.ajax({
    type     :'POST',
    cache    : false,
    url  : "<?php echo Url::toRoute(['getotdeli']);?>",  
    dataType: 'html',
    data: {city_id: n},
    success  : function(response) {  
     $('#nova_poshta').html(response);
   }
 });
 }







      $(document).ready(function () {


       


$( "#city" ).change(function() {    
      city($("select#city").val());   
  });

$(function () {
  // инициализировать все элементы на страницы, имеющих атрибут data-toggle="tooltip", как компоненты tooltip
  $('[data-toggle="tooltip"]').tooltip()
})


 country(2);


    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();
    
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

      var $target = $(e.target);

      if ($target.parent().hasClass('disabled')) {
        return false;
      }
    });

    $(".next-step").click(function (e) {

      var $active = $('.wizard .nav-tabs li.active');
      $active.next().removeClass('disabled');
      nextTab($active);

    });
    $(".prev-step").click(function (e) {

      var $active = $('.wizard .nav-tabs li.active');
      prevTab($active);

    });
  });

      function nextTab(elem) {
        $(elem).next().find('a[data-toggle="tab"]').click();
      }
      function prevTab(elem) {
        $(elem).prev().find('a[data-toggle="tab"]').click();
      }


//according menu

$(document).ready(function()
{
    //Add Inactive Class To All Accordion Headers
    $('.accordion-header').toggleClass('inactive-header');

  //Set The Accordion Content Width
  var contentwidth = $('.accordion-header').width();
  $('.accordion-content').css({});
  
  //Open The First Accordion Section When Page Loads
  $('.accordion-header').first().toggleClass('active-header').toggleClass('inactive-header');
  $('.accordion-content').first().slideDown().toggleClass('open-content');
  
  // The Accordion Effect
  $('.accordion-header').click(function () {
    if($(this).is('.inactive-header')) {
      $('.active-header').toggleClass('active-header').toggleClass('inactive-header').next().slideToggle().toggleClass('open-content');
      $(this).toggleClass('active-header').toggleClass('inactive-header');
      $(this).next().slideToggle().toggleClass('open-content');
    }
    
    else {
      $(this).toggleClass('active-header').toggleClass('inactive-header');
      $(this).next().slideToggle().toggleClass('open-content');
    }
  });
  
  return false;
});

</script>



<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAMzRlocBxBIvZEkONthBf0RPbNT4dx0t8&signed_in=true&libraries=places&callback=initAutocomplete"
async defer></script>


<script>
// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

var placeSearch1, autocomplete1;
var componentForm1 = {
  street_number1: 'short_name',
  route1: 'long_name',
  locality1: 'long_name',  
  country1: 'long_name',
 
};




var placeSearch, autocomplete;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',  
  country: 'long_name',
  
};

function initAutocomplete() {
  // Create the autocomplete object, restricting the search to geographical
  // location types.
  //autocomplete = new google.maps.places.Autocomplete(
    /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
   // {types: ['geocode']});

  // When the user selects an address from the dropdown, populate the address
  // fields in the form.
  //autocomplete.addListener('place_changed', fillInAddress);


  //forma2
  autocomplete1 = new google.maps.places.Autocomplete(
    /** @type {!HTMLInputElement} */(document.getElementById('autocomplete1')),
    {types: ['geocode']});
  autocomplete1.addListener('place_changed', fillInAddress1);
}

// [START region_fillform]
function fillInAddress() {
  $('#street_number').val('');
$('#route').val('');
$('#locality').val('');
$('#country').val('');
$('#postal_code').val('');
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  for (var component in componentForm) {
    //document.getElementById(component).value = '';
    //document.getElementById("street_number").disabled = false;
    //document.getElementById(component).disabled = false;
  }

  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];

    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
    
  }
}


function fillInAddress1() {
  $('#street_number1').val('');
$('#route1').val('');
$('#locality1').val('');
$('#country1').val('');
$('#postal_code1').val('');
   var place = autocomplete1.getPlace();

  for (var component in componentForm1) {
    //document.getElementById(component).value = '';
    //document.getElementById("street_number1").disabled = false; 
    //document.getElementById(component).disabled = false;   
  }

  
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];  


    if (componentForm[addressType]) {
      var idw = addressType+'1';
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(idw).value = val;
    }
   
  }

}
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete.setBounds(circle.getBounds());
    });
  }
}
// [END region_geolocation]



function geolocate1() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete1.setBounds(circle.getBounds());
    });
  }
}

</script>






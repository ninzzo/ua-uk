<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

//use kartik\date\DatePicker;
use yii\helpers\Url;

use yii\widgets\MaskedInput;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;

/* @var $this yii\web\View */
$this->title = Yii::$app->name;
?>
  <div class="row">
    <div class="col-lg-2">


    </div>

    
    <div class="col-lg-7">




      <?php 
      
      $form = ActiveForm::begin(); 


      ?>
      <?= $form->errorSummary($model); ?>
     
 


      <div class="panel-group" id="accordion">
        <!-- 1 панель -->
        <div class="panel panel-default panel-accordion">
          <!-- Заголовок 1 панели -->
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" id="collapse1" href="#collapseOne"><?php echo Yii::t('app', 'zakaz_dostavki_to:');?><i class="indicator glyphicon glyphicon-chevron-down  pull-right"></i></a>
              
            </h4>
          </div>
          <div id="collapseOne" class="panel-collapse collapse">
            <!-- Содержимое 1 панели -->
            <div class="panel-body">
              <div id="to_en">

                <?= $form->field($model, 'en_full_adress')->textInput(['id'=>'autocomplete', 'onFocus'=>"geolocate()"])->label("Postcode or adress") ?>
                <div class="row">
                  <div class="col-lg-3">
                    <?= $form->field($model, 'en_street_number')->textInput(['id'=>'street_number','disabled'=>true])->label("House number") ?>
                  </div>
                  <div class="col-lg-9">
                    <?= $form->field($model, 'en_street_name')->textInput(['id'=>'route','disabled'=>true])->label("Street address") ?>
                  </div>
                </div>


                <div class="row">
                 <div class="col-lg-4">
                  <?= $form->field($model, 'en_city')->textInput(['id'=>'locality','disabled'=>true])->label("City") ?>
                </div>
                <div class="col-lg-4">
                  <?= $form->field($model, 'en_postal_code')->textInput(['id'=>'postal_code','disabled'=>true])->label("PostCode") ?>
                </div>
                <div class="col-lg-4">
                  <?= $form->field($model, 'en_country')->textInput(['id'=>'country','disabled'=>true])->label("Country") ?>
                </div>
              </div>



            </div>

            
              <div class="row">
                <div class="col-lg-4">
                 
                  <?= $form->field($model, 'to_firstname')->textInput() ?>
                  </div>

                  <div class="col-lg-4">
                  <?= $form->field($model, 'to_lastname')->textInput() ?>

                </div>

                </div>

                <div class="row">
                <div class="col-lg-4">
                 <?= $form->field($model, 'tel')->widget(\yii\widgets\MaskedInput::className(), [
    'mask' => '+9(9999)999-9999',
    'clientOptions' => [        
        'removeMaskOnSubmit' => true,
        ]
])->textInput(['data-toggle'=>'tooltip', 'title'=>"В международном формате. 12 цифр", 'data-placement'=>"top"])->label("Тел1 получателя") ?>


                </div>

                <div class="col-lg-4">
                  <?= $form->field($model, 'to_tel2')->textInput()->widget(\yii\widgets\MaskedInput::className(), [
    'mask' => '+9(9999)999-9999',
    'clientOptions' => [        
        'removeMaskOnSubmit' => true,
        ]
])->label("Тел2 получателя") ?>
                </div>

              </div>


              <?= $form->field($model, 'to_desc')->textarea(['rows' => 2, 'cols' => 5]) ?>

<div class="row">
 <div class="col-lg-12">
         <a style="float:right;" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="btn btn-info btn-xs" aria-expanded="true">Далее</a>
            </div>
            </div>

            </div>
          </div>
        </div>
        <!-- 2 панель -->
        <div class="panel panel-default  panel-accordion">
          <!-- Заголовок 2 панели -->
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><?php echo Yii::t('app', 'o_posilke');?><i class="indicator glyphicon glyphicon-chevron-down  pull-right"></i></a>
                          </h4>
          </div>
          <div id="collapseTwo" class="panel-collapse collapse">
            <!-- Содержимое 2 панели -->
            <div class="panel-body">



            <div class="row">
 <div class="col-lg-12">
         <a style="float:right;" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="btn btn-info btn-xs" aria-expanded="true">Назад</a>
            </div>
            </div>


          
             <div class="row">
              <div class="col-lg-3">
                <?php $model->parcel_ves = 1;?>
                <?= $form->field($model, 'parcel_ves')->textInput(['type' => 'number','min'=>1, 'id'=>'ves','data-toggle'=>'tooltip', 'title'=>"Цена посылки зависит от веса. Больше 10 КГ - скидки!", 'data-placement'=>"top"])->label('Вес посылки(КГ)') ?>

              </div>
              <div class="col-lg-3">
                <?php $model->parcel_visota = 1;?>
                <?= $form->field($model, 'parcel_visota')->textInput(['type' => 'number','min'=>1, 'data-toggle'=>'tooltip', 'title'=>"Количество сумок и пакетов!", 'data-placement'=>"top"])->label('Нужно мест') ?>
              </div>

              <div class="col-lg-3">
                <?= $form->field($model, 'parcel_wirina')
                    ->checkbox([
                        'label' => Yii::t('app', 'est_novie_vewi'),
                        'labelOptions' => [
                            'style' => 'padding-top:30px;'
                        ],
                     
                    ]); ?>
              </div>

            </div>

              <?= $form->field($model, 'parcel_dlina')->textarea(['rows' => 2, 'cols' => 5, 'placeholder' => 'Opiwite posilku svoimi slovami ...'])->label(Yii::t('app', 'opisanie posilki')) ?>

            <h2><?php echo Yii::t('app', 'Ocenka gruza');?> - £ <span id="cena">1</span></h2> 


   <div class="row">
 <div class="col-lg-12">
         <a style="float:right;" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="btn btn-info btn-xs" aria-expanded="true">Далее</a>
            </div>
            </div>

           

          </div>
        </div>
      </div>
      <!-- 3 панель -->
      <div class="panel panel-default  panel-accordion">
        <!-- Заголовок 3 панели -->
        <div class="panel-heading">
          <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><?php echo Yii::t('app', 'gde_zabrat_parcel');?>
      <i class="indicator glyphicon glyphicon-chevron-down  pull-right"></i>
            </a>

          </h4>
        </div>
        <div id="collapseThree" class="panel-collapse collapse">
          <!-- Содержимое 3 панели -->
          <div class="panel-body">

            <div class="row">
 <div class="col-lg-12">
         <a style="float:right;" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="btn btn-info btn-xs" aria-expanded="true">Назад</a>
            </div>
            </div>

           

   <?= $form->field($model, 'en_full_adress')->textInput(['id'=>'autocomplete1', 'onFocus'=>"geolocate1()"])->label("Postcode or adress") ?>
                <div class="row">
                  <div class="col-lg-3">
                    <?= $form->field($model, 'from_paradniy')->textInput(['id'=>'street_number1','disabled'=>true])->label("House number") ?>
                  </div>
                  <div class="col-lg-9">
                    <?= $form->field($model, 'from_street')->textInput(['id'=>'route1','disabled'=>true])->label("Street address") ?>
                  </div>
                </div>


                <div class="row">
                 <div class="col-lg-4">
                  <?= $form->field($model, 'from_city')->textInput(['id'=>'locality1','disabled'=>true])->label("City") ?>
                </div>
                <div class="col-lg-4">
                  <?= $form->field($model, 'from_postcode')->textInput(['id'=>'postal_code1','disabled'=>true])->label("PostCode") ?>
                </div>
                <div class="col-lg-4">
                  <?= $form->field($model, 'from_country')->textInput(['id'=>'country1','disabled'=>true])->label("Country") ?>
                </div>
                </div>

           
            <div class="row">   
             <div class="col-lg-4">
              <?= $form->field($model, 'from_firstname')->textInput() ?>
              </div>
              <div class="col-lg-4">
              <?= $form->field($model, 'from_lastname')->textInput() ?>
            </div>
            <div class="col-lg-4">
              <?= $form->field($model, 'from_mail')->textInput() ?>
            </div>
             </div>


            <div class="row">   
            <div class="col-lg-4">
              <?= $form->field($model, 'from_tel1')->widget(\yii\widgets\MaskedInput::className(), [
    'mask' => '+9(9999)999-9999',
    'clientOptions' => [        
        'removeMaskOnSubmit' => true,
        ]
])->textInput(['data-toggle'=>'tooltip', 'title'=>"В международном формате. 12 цифр", 'data-placement'=>"top"])->label("Тел1 отправителя") ?>
            </div>
            <div class="col-lg-4">
              <?= $form->field($model, 'from_tel2')->widget(\yii\widgets\MaskedInput::className(), [
    'mask' => '+9(9999)999-9999',
    'clientOptions' => [        
        'removeMaskOnSubmit' => true,
        ]
])->textInput()->label("Тел2 отправителя") ?>
            </div>
          </div>


<div class="row">  
<div class="col-lg-6">
          <?= $form->field($model,'from_time1')->widget(DateTimePicker::className(),['options' => ['placeholder' => 'Select from time'],'pluginOptions' => ['format' => 'dd/MM/yyyy H:i', 'autoclose'=>true,]]) ?>
  </div>
            <div class="col-lg-6">
           <?= $form->field($model,'from_time2')->widget(DateTimePicker::className(),['options' => ['placeholder' => 'Select to time'],'pluginOptions' => ['format' => 'dd/MM/yyyy H:i', 'autoclose'=>true,]]) ?>
    </div>
          </div>

           <br>

           <?= $form->field($model, 'from_desc')->textarea(['rows' => 2, 'cols' => 5]) ?>
           <div class="form-group">
           <center>
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-lg' : 'btn btn-primary btn-lg']) ?><center>
          </div>

        </div>
      </div>
    </div>
  </div>

  <?php ActiveForm::end(); ?>

</div>


<div class="col-lg-3">

  <!--  <div>
<div class="col-lg-3 ugli" style="background:white!important;">

  <h3>Все стало еще проще:</h3>   
      
<span class="chek">✔</span> Заполните краткую форму  <br>  
<span class="chek">✔</span> Укажите удобную дату и время сбора посылок  <br>   
<span class="chek">✔</span> Ожидайте подтверждение СМС и письмом на вашу электронную почту  <br>   
<span class="chek">✔</span> Отследить посылку   <br> 
<span class="chek">✔</span> Оставьте свой комментарий   <br> 
</div>
    </div>-->

</div>


</div></div>
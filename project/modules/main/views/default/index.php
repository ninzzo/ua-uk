<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;


/* @var $this yii\web\View */
$this->title = 'Главная';
?>

    <div class="wrap-slider">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

            <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
            </ol>
 

            <div class="carousel-inner">
                <div class="item active">
                    <img src="<?php echo Yii::getAlias('@web'); ?>/image/ukr1.jpg" >

                    <div class="carousel-caption">

                        <h3></h3>
                    </div>
                </div>
                <div class="item">
                    <img src="<?php echo Yii::getAlias('@web'); ?>/image/banner-eng.jpg" >

                    <div class="carousel-caption">
                        <h3></h3>
                    </div>
                </div>

                <div class="item">
                    <img src="<?php echo Yii::getAlias('@web'); ?>/image/mob_banner.jpg" >

                    <div class="carousel-caption">
                        <h3></h3>
                    </div>
                </div>

            </div>
            <a class="left carousel-control" href="#carousel-example-generic" role="button"
               data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button"
               data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>

        </div>
    </div>

    <div class="wrap-body">
        <div class="container">
            <div class="row">
                <div class=" col-md-4 col-sm-4 col-xs-12 ">
                    <div class="btn-group hidden-lg hidden-md  hidden-sm col-xs-offset-1">
                        <a href="#" class="btn btn-link uk-ua">
                        </a>
                    </div>
                    <div class=" col-md-6 col-sm-7 col-xs-6   col-xs-push-3">
                        <img class="img-responsive britan" src="<?php echo Yii::getAlias('@web'); ?>/image/mapUK.png" alt="">
                    </div>
                    <div class=" col-md-3 col-sm-3 col-xs-3  col-sm-pull-3 col-xs-pull-5   col-lg-push-3 col-md-push-3 col-sm-push-3 ">
                        <img class="img-responsive buttonAll" src="<?php echo Yii::getAlias('@web'); ?>/image/buttonUp.png" alt="">
                    </div>
                    <div class=" col-xs-3  hidden-sm hidden-md hidden-lg ">
                        <img class="img-responsive buttonAll" src="<?php echo Yii::getAlias('@web'); ?>/image/buttonDown.png" alt="">
                    </div>
                    <div class="btn-group  hidden-xs pull-right">
                    <?= Html::a('', ['/uktoua/create'],['class'=>'btn btn-link uk-ua']) ?>
                       
                        </a>
                    </div>
                </div>


                <div class="col-lg-4 col-sm-4 col-xs-12 center-block">

                    <img src="<?php echo Yii::getAlias('@web'); ?>/image/carMan.png" alt="" class="img-responsive carMan">
                    <img class="img-responsive carText" src="<?php echo Yii::getAlias('@web'); ?>/image/rightOrder.png" alt="">

                </div>


                <div class="col-lg-4 col-sm-4 col-xs-12 ">
                    <div class=" col-xs-3  hidden-sm hidden-md hidden-lg  col-xs-push-1">
                        <img class="img-responsive buttonAll" src="<?php echo Yii::getAlias('@web'); ?>/image/buttonUp.png" alt="">
                    </div>

                    <div class=" col-lg-3  col-md-3 col-sm-3 col-xs-3 col-sm-push-6 col-xs-push-6 col-md-push-1 col-lg-push-1 col-sm-push-1">
                        <img class="img-responsive buttonAll" src="<?php echo Yii::getAlias('@web'); ?>/image/buttonDown.png" alt="">
                    </div>

                    <div class="col-md-7 col-sm-8 col-xs-6 col-sm-pull-3 col-xs-pull-3 col-lg-push-1 col-md-push-1 col-sm-push-1">
                        <img class="img-responsive" src="<?php echo Yii::getAlias('@web'); ?>/image/mapUA.png" alt="">
                    </div>
                    <div class="btn-group col-xs-offset-1 col-sm-offset-0">
                        <a href="#" class="btn btn-link ua-uk">

                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="wrap-easy">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="text-center text-uppercase">
                        <h1 class="grandText lead"><span class="none">  &mdash;&mdash;&mdash; </span>  НАЧАТЬ ОТПРАВЛЯТЬ ПРОСТО  <span class="none">  &mdash;&mdash;&mdash; </span></h1>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-4 col-xs-12 but">
                    <div class="col-lg-6 col-sm-6 col-xs-4">
                        <img class="img-responsive " src="<?php echo Yii::getAlias('@web'); ?>/image/one.png" alt="one">
                    </div>
                    <div class="col-lg-6 col-sm-6 col-xs-8 ">
                        <p class="butText"> Расчитайте стоимость</p>
                        <a href="#"   class=" butLink" >  Калькулятор   </a>
                    </div>

                </div>

                <div class="col-lg-4 col-sm-4 col-xs-12 but">
                    <div class="col-lg-6 col-sm-6 col-xs-4">
                        <img class="img-responsive " src="<?php echo Yii::getAlias('@web'); ?>/image/two.png" alt="two">
                    </div>
                    <div class="col-lg-6 col-sm-6 col-xs-8">
                        <p class="butText"> Вызовите  курьера  </p>
                            <a href="#" class=" butLink">Оставить заявку </a>
                    </div>
                </div>


                <div class="col-lg-4 col-sm-4 col-xs-12 but">
                    <div class="col-lg-6 col-sm-6 col-xs-4">
                                <img class="img-responsive " src="<?php echo Yii::getAlias('@web'); ?>/image/three.png" alt="three">
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-8">
                            <p class="butText"> Подготовьте  отправление </p>
                                <a href="#"  class=" butLink" >   Узнать об упаковке </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <div class="wrap-work">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <img class="img-responsive" src="<?php echo Yii::getAlias('@web'); ?>/image/HDIW%20.png" alt="howDoesItWork">
                </div>
            </div>
        </div>
    </div>

    <div class="wrap-read">
        <div class="container">
            <div class="row">

                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="text-center text-uppercase ">
                        <h1 class="grandText lead"> <span class="none">&mdash;&mdash;&mdash; </span>  ПОЛЕЗНО ПОЧИТАТЬ  <span class="none">  &mdash;&mdash;&mdash; </span> </h1>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-10 col-xs-offset-1 col-sm-offset-0 read-marg">
                    <div class="read-img">
                        <img class="img-responsive carReed" src="<?php echo Yii::getAlias('@web'); ?>/image/read1.png" alt="read1">
                    </div>
                    <div class="read-write">
                        <p class="read-data"> 12 Января 2016 </p>

                        <h3 class="read-chapter">
                            Доставка становится выгоднее – в 2 раза дешевле, чем USPS!
                        </h3>

                        <p class="read-text">
                            Представляем новую выгодную доставку от Posti-EMS.
                        </p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-10 col-xs-offset-1 col-sm-offset-0 read-marg">
                    <div class="read-img">
                        <img class="img-responsive carReed" src="<?php echo Yii::getAlias('@web'); ?>/image/read2.png" alt="read1">
                    </div>
                    <div class="read-write">
                        <p class="read-data"> 15 Апреля 2016 </p>

                        <h3 class="read-chapter">
                            Регистрируйте посылку заранее, экономьте на комиссии!
                        </h3>

                        <p class="read-text">
                            Если Вы забываете указать ваш номер room и зарегистрировать посылку, то этот заказ...
                        </p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-10 col-xs-offset-1 col-sm-offset-0 read-marg">
                    <div class="read-img">
                        <img class="img-responsive carReed" src="<?php echo Yii::getAlias('@web'); ?>/image/read3.png" alt="read1">
                    </div>
                    <div class="read-write">
                        <p class="read-data"> 02 Июня 2016</p>

                        <h3 class="read-chapter">
                            Дорогие друзья! Вам подарок – лето за полцены!
                        </h3>

                        <p class="read-text">
                            Чтобы этот жаркий летний сезон стал еще лучше, мы решили сделать для всех клиентов...
                        </p>
                    </div>
                </div>

                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <button type="button" class="btn btn-link center-block orderH">
                        <div class="btn-group">
                            <a href="#" class="btn btn-link center-block order" role="button">
                            </a>
                        </div>
                    </button>
                </div>
            </div>
        </div>
    </div>

    









<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

//use kartik\date\DatePicker;
use yii\helpers\Url;

use yii\widgets\MaskedInput;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;

/* @var $this yii\web\View */
$this->title = Yii::$app->name;
?>
<style>
.panel-accordion .panel-heading {
    padding: 0;
}
.panel-accordion .panel-heading a {
    display: block;
    padding: 10px 15px;
}
.chek{color:green;
font-size:24px;}

.ugli{

border-radius:6px;  
-moz-border-radius:5px; 
-khtml-border-radius:10px;
border: 1px solid black;
}
.title1 {

  font-size: 24px;
   margin-top: -50px;
 }

 .text1 {

  max-width: 250px;
 }


   .infographic .box:before {
    width: 73px;
    height: 72px;
    margin: 40px auto 10px;
    display: block;
    content: '';
    background-image: url(https://www.remontnik.ru/static/images/sprite-how-it-works.png);
    background-repeat: no-repeat;
}

.infographic .box.compare:before {
    background-position: -83px 0;
}

.infographic .box.agree:before {
    background-position: 0 -82px;
}


</style>



<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>
 
  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="/2222.png" style="width:1140px;height:299px;" alt="...">
      <div class="carousel-caption">
      	
      	<h3>Caption Text</h3>
      </div>
    </div>
    <div class="item">
      <img src="http://placehold.it/1200x315" alt="...">
      <div class="carousel-caption">
      	<h3>Caption Text</h3>
      </div>
    </div>
    <div class="item">
      <img src="http://placehold.it/1200x315" alt="...">
      <div class="carousel-caption">
      	<h3>Caption Text</h3>
      </div>
    </div>
  </div>
 
  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
</div> <!-- Carousel -->



<div class="main-default-index">
<br>
<div class="row">
	<div class="col-lg-1">

    </div>
    <div class="col-lg-5">
    		<center><a href="#" style="width:340px;" class="btn btn-primary btn-lg">Заказать перевозку England-Ukraine</a></center>

    </div>

    <div class="col-lg-5">
    		<center><a href="#" style="width:340px;" class="btn btn-info btn-lg">Заказать перевозку Ukraine-England</a></center>


    </div>
    <div class="col-lg-1">
	
    </div>

    </div>
    <br>



<div class="row previews">
        <div class="col-lg-4 col-sm-6">
            <div class="thumbnail">
            <div class="infographic">
                <div class="box add">
                            <center><span class="title1">Добавьте заказ</span><br>
                            <span class="text1">Заполните простую форму ниже: что, от куда и куда хотите доставить</span></center>
                        </div></div><br>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="thumbnail">
            <div class="infographic">
                <div class="box compare">
                            <center><span class="title1">Получите SMS подтверждение</span><br>
                            <span class="text1">Наш курьер в ближайшее время подтвердит готовность забрать посылку</span></center>
                        </div></div><br>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="thumbnail">
                <div class="infographic">
                <div class="box agree">
                            <center><span class="title1">Передайте посылку</span><br>
                            <span class="text1">Курьер подъедет в указанное время по вашему адресу и заберет посылку</span></center>
                        </div>
                      </div>  <br>            
            </div>
        </div>
    </div>


</div>


<script>



  function country(n){

   $.ajax({
    type     :'POST',
    cache    : false,
    url  : "<?php echo Url::toRoute(['getcity']);?>",
    dataType: 'html',
    data: {country_id: n},
    success  : function(response) {     
     $('#city').html(response);
   }
 });
 }

 function city(n){

   $.ajax({
    type     :'POST',
    cache    : false,
    url  : "<?php echo Url::toRoute(['getotdeli']);?>",  
    dataType: 'html',
    data: {city_id: n},
    success  : function(response) {  
     $('#nova_poshta').html(response);
   }
 });
 }


 $( document ).ready(function() {


 	$(function () {
  // инициализировать все элементы на страницы, имеющих атрибут data-toggle="tooltip", как компоненты tooltip
  $('[data-toggle="tooltip"]').tooltip()
})

function toggleChevron(e) {
    $(e.target)
        .prev('.panel-heading')
        .find("i.indicator")
        .toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
}
$('#accordion').on('hidden.bs.collapse', toggleChevron);
$('#accordion').on('shown.bs.collapse', toggleChevron);



  $( "#to_country" ).change(function() {    

 

 /*$('#collapseOne').collapse("hide");
 $('#collapseTwo').collapse("hide");
 $('#collapseThree').collapse("hide");*/

 $('#collapseOne').collapse("show");
    //узнаем высоту от начала страницы до блока на который ссылается якорь
    var top = $('#collapse1').offset().top;
    var top = top - 150;
    //анимируем переход на расстояние - top за 1500 мс
    $('body,html').animate({scrollTop: top}, 500);

    //$('#to_country').removeAttr('placeholder');
    //$("#to_country :first").remove();
    $("#to_country [value='']"). remove();




    if($("select#to_country").val() == 2){
      $("#to_country [value='2']").prop("selected", true);
      country(2);
       $('#to_en').hide();
      $('#to_ukr').show();
    
      
    }
    
    if($("select#to_country").val() == 1){

      $("#to_country [value='1']").prop("selected", true);
      country(1);

      $('#to_en').show();
      $('#to_ukr').hide();

    }
  });



    	




  $( "#city" ).change(function() { 
    if($("select#to_country").val() == 2){
      city($("select#city").val());
    }
  });


  $( "#ves" ).change(function() {
    if($("#ves").val() < 10){$("#cena").html(2.5*$("#ves").val());}
    if($("#ves").val() >= 10 && $("#ves").val() < 20){$("#cena").html(2*$("#ves").val());}
    if($("#ves").val() >= 20){$("#cena").html(1.5*$("#ves").val());}
  });

//start page    
country(2);

});


</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAMzRlocBxBIvZEkONthBf0RPbNT4dx0t8&signed_in=true&libraries=places&callback=initAutocomplete"
async defer></script>


<script>
// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

var placeSearch1, autocomplete1;
var componentForm1 = {
  street_number1: 'short_name',
  route1: 'long_name',
  locality1: 'long_name',  
  country1: 'long_name',
  postal_code1: 'short_name'
};




var placeSearch, autocomplete;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',  
  country: 'long_name',
  postal_code: 'short_name'
};

function initAutocomplete() {
  // Create the autocomplete object, restricting the search to geographical
  // location types.
  autocomplete = new google.maps.places.Autocomplete(
    /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
    {types: ['geocode']});

  // When the user selects an address from the dropdown, populate the address
  // fields in the form.
  autocomplete.addListener('place_changed', fillInAddress);


  //forma2
  autocomplete1 = new google.maps.places.Autocomplete(
    /** @type {!HTMLInputElement} */(document.getElementById('autocomplete1')),
    {types: ['geocode']});
  autocomplete1.addListener('place_changed', fillInAddress1);
}

// [START region_fillform]
function fillInAddress() {
  $('#street_number').val('');
$('#route').val('');
$('#locality').val('');
$('#country').val('');
$('#postal_code').val('');
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  for (var component in componentForm) {
    document.getElementById(component).value = '';
    document.getElementById("street_number").disabled = false;
    document.getElementById(component).disabled = false;
  }

  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];

    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
    $( "#street_number" ).focus();
  }
}


function fillInAddress1() {
  $('#street_number1').val('');
$('#route1').val('');
$('#locality1').val('');
$('#country1').val('');
$('#postal_code1').val('');
   var place = autocomplete1.getPlace();

  for (var component in componentForm1) {
    document.getElementById(component).value = '';
    document.getElementById("street_number1").disabled = false; 
    document.getElementById(component).disabled = false;   
  }

  
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];  


    if (componentForm[addressType]) {
      var idw = addressType+'1';
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(idw).value = val;
    }
    $( "#street_number1" ).focus();
  }

}
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete.setBounds(circle.getBounds());
    });
  }
}
// [END region_geolocation]



function geolocate1() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete1.setBounds(circle.getBounds());
    });
  }
}







</script>

 
<!--AIzaSyD65IU2SRdBlMcZ1_2t4fiATm2XvLN0-yQ-->


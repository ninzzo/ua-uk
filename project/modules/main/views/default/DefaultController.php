<?php

namespace app\modules\main\controllers;

use yii\web\Controller;
use app\modules\main\models\City;
use app\modules\main\models\Towns;
use yii\helpers\ArrayHelper;
use Yii;


class DefaultController extends Controller
{
    public function actions()
    {
        return [
        'error' => [
        'class' => 'yii\web\ErrorAction',
        ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionTest1()
    {
        return $this->render('test1');
    }

    public function actionTest2()
    {      
        
        //$result = ArrayHelper::map(City::find()->where(['country_id' => 2])->all(), 'id', 'name_ru');
        return $this->render('test2');
    }

    public function actionTest3()
    {      
        
        //$result = ArrayHelper::map(City::find()->where(['country_id' => 2])->all(), 'id', 'name_ru');
        return $this->render('test3');
    }


    public function actionGetcity()
    {


      if (Yii::$app->request->isAjax) {
       $data = Yii::$app->request->post();

       if($data[country_id] == 2){       
         $city = City::find()->where(['country_id' => 2])->all();
         foreach ($city as $key) {
          echo '<option value="'.$key[id].'">'.$key[name_ru].'</option>'; 
        } 
      }

        /*if($data[country_id] == 1){
         $city = Towns::find()->all();
         foreach ($city as $key) {
          echo '<option value="'.$key[town_id].'">'.$key[town].'</option>';   
        }      
      }*/

    

    }
}

public function actionGetotdeli()
{


    if (Yii::$app->request->isAjax) {
       $data = Yii::$app->request->post();
       $city = City::find()    
       ->where(['id' => $data[city_id]])
       ->one();


       $ref = $city[ref];         
       
       $json = '{
           "apiKey": "be71eb8db8fc598064fa2637774c6965",
           "modelName": "Address",
           "calledMethod": "getWarehouses",
           "methodProperties": {
               "CityRef": "'.$ref.'"
           }
       }';

       $result = file_get_contents('https://api.novaposhta.ua/v2.0/json/', null, stream_context_create(array(
        'http' => array(
            'method' => 'POST',
            'header' => "Content-type: application/x-www-form-urlencoded;\r\n",
            'content' => $json,
            ),
        )));

       $result = json_decode($result, true);
       $result = $result[data];
       foreach ($result as $key) {   
        echo '<option value="'.$key[DescriptionRu].'">'.$key[DescriptionRu].'</option>';        
    }

}
}


}

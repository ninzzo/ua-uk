<?php

namespace app\modules\main\controllers;

use yii\web\Controller;
use app\modules\main\models\City;
use app\modules\main\models\form\ContactForm;
use app\modules\zakaz\models\Zakaz;
use app\modules\user\models\User;
use yii\helpers\ArrayHelper;
use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;








class DefaultController extends Controller
{

     public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
        'error' => [
        'class' => 'yii\web\ErrorAction',
        ],
        ];
    }


    public function actionGettoken()
    {
        if (Yii::$app->request->post()) {
            $data = Yii::$app->request->post();  
           
            $user  = User::find()->where(['username'=>$data[login]])->one();                   
            if(!empty($user) AND Yii::$app->security->validatePassword($data[pass], $user->password_hash)){
                echo $user->auth_key;
            }else{
                echo '0';
            }       
        }
    }




    public function send_sms($to, $body){
           //    curl -H "Authorization: Bearer a8e3911d7b744f50b95e80db0a727cee" \
       /* $tel = $to;
        $tel = str_replace("(","",$tel);
        $tel = str_replace(")","",$tel);
        $tel = str_replace("-","",$tel);

        $url ='https://api.mblox.com/xms/v1/uaukz12/batches';
        $json = array("from"=>"UA-UK","to"=>array($tel), "body" => $body);
        $json = json_encode($json);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($json),
            'Authorization: Bearer a8e3911d7b744f50b95e80db0a727cee')
        );
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        $result = curl_exec($ch);
        curl_close($ch);*/

    //echo $result;

    }


     public function send_mail($to, $body){
        $mailer = Yii::$app->get('mailer');
        $message = $mailer->compose()
        ->setTo($to)
        ->setFrom("info@ua-uk.com")
        ->setSubject("Ua-Uk info")
        ->setTextBody($body)
        ->send();

     }


     public function coord($adres){   

     $adres = urlencode($adres);
$adr = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$adres.'&key=AIzaSyD65IU2SRdBlMcZ1_2t4fiATm2XvLN0-yQ';
            $result = file_get_contents($adr, false);

                $result = json_decode($result, true);

                $x = $result[results][0][geometry][location][lat];
                $y = $result[results][0][geometry][location][lng];
                if(isset($x) and isset($y)){return $x.', '.$y;}

     }


     public function actionAbout()
    {
    	return $this->render('about');

    }

    public function actionHowmuch()
    {
        return $this->render('howmuch');

    }


    

    public function actionPrice()
    {
    	return $this->render('price');
    }

    public function actionRules()
    {

        $model = new ContactForm();
        
        if ($user = Yii::$app->user->identity) {
            /** @var \app\modules\user\models\User $user */
            $model->name = $user->username;
            $model->email = $user->email;
        }
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');
            return $this->refresh();
        } 

    	return $this->render('rules', [
                'model' => $model,
            ]);
    }

    public function actionHowwork()
    {
    	return $this->render('howwork');
    }


    public function actionIndex()
    {
			


   
            return $this->render('index');
              
        
    }

    protected function findModel($id)
    {
        if (($model = Zakaz::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionUauk()
    {
        $model = new Zakaz();

        $model->user_id = '1';
        $model->created_at = time();

        if ($user = Yii::$app->user->identity) {
           
          
        
        $model->from_firstname = $user->firstname;
        $model->from_lastname = $user->lastname;
        $model->from_tel1 = substr($user->tel, 2);
        $model->from_mail = $user->email;
        }

        $model->to_county = 1;

        
        $model->scenario = 'ua_uk';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {    
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'VAW_ZAKAZ_PRINYAT'));      
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('ua-uk', [
                'model' => $model,
            ]);
        }  
    }

    public function actionUkua()
    {
        Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/uploads/';
        $model = new Zakaz();
        

        $model->user_id = '1';                

        if ($user = Yii::$app->user->identity) {
               
        
        $model->from_firstname = $user->firstname;
        $model->from_lastname = $user->lastname;
        $model->from_tel1 = substr($user->tel, 2);

        $model->from_mail = $user->email;
        }
        
        $model->scenario = 'uk_ua';

         if ($model->load(Yii::$app->request->post())) {
            

            //images
            $image = UploadedFile::getInstance($model, 'image'); 
            $image2 = UploadedFile::getInstance($model, 'image2');   

            if(!empty($image->name)){
                $model->image = $image->name;
                $ext = end((explode(".", $image->name)));
                $model->image = Yii::$app->security->generateRandomString().".{$ext}";
                $path = Yii::$app->params['uploadPath'] . $model->image;
            }
            if(!empty($image2->name)){
                $model->image2 = $image2->name;
                $ext2 = end((explode(".", $image2->name)));
                $model->image2 = Yii::$app->security->generateRandomString().".{$ext2}";
                $path2 = Yii::$app->params['uploadPath'] . $model->image2;
            }

            $model->created_at = time();            
            $model->to_county = 2;

                    $adres = '';
                    $adres.= $model->from_country.',';
                    $adres.= '+'.$model->from_city.',';
                    $adres.= '+'.$model->from_street.',';
                    $adres.= '+'.$model->from_paradniy.',';  
            $model->coords = $this->coord($adres);

            if($model->save()){
                if(!empty($image->name)){
                $image->saveAs($path);                    
                }

                if(!empty($image2->name)){
                $image2->saveAs($path2);                    
                }


                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'VAW_ZAKAZ_PRINYAT'));   

                  $this->send_sms($model->from_tel1,'Спасибо ваш заказ создан! Ожидайте подтверждение');
                 $this->send_mail($model->from_mail,'Спасибо ваш заказ создан! Ожидайте подтверждение');   
                 return $this->redirect(['index', 'id' => $model->id]);
                
            } else {
                // error in saving model
            }
        }else{

             return $this->render('uk-ua', [
                'model' => $model,
            ]);
        }

    }
  

    

    public function actionGetcity()
    {


      if (Yii::$app->request->isAjax) {
       $data = Yii::$app->request->post();

       if($data[country_id] == 2){       
         $city = City::find()->where(['country_id' => 2])->all();
         foreach ($city as $key) {
          echo '<option value="'.$key[id].'">'.$key[name_ru].'</option>'; 
        } 
      }

        

    

    }
}

public function actionGetotdeli()
{


    if (Yii::$app->request->isAjax) {
       $data = Yii::$app->request->post();
       $city = City::find()    
       ->where(['id' => $data[city_id]])
       ->one();


       $ref = $city[ref];         
       
       $json = '{
           "apiKey": "be71eb8db8fc598064fa2637774c6965",
           "modelName": "Address",
           "calledMethod": "getWarehouses",
           "methodProperties": {
               "CityRef": "'.$ref.'"
           }
       }';

       $result = file_get_contents('https://api.novaposhta.ua/v2.0/json/', null, stream_context_create(array(
        'http' => array(
            'method' => 'POST',
            'header' => "Content-type: application/x-www-form-urlencoded;\r\n",
            'content' => $json,
            ),
        )));

       $result = json_decode($result, true);
       $result = $result[data];
       foreach ($result as $key) {   
        echo '<option value="'.$key[DescriptionRu].'">'.$key[DescriptionRu].'</option>';        
    }

}
}


}
